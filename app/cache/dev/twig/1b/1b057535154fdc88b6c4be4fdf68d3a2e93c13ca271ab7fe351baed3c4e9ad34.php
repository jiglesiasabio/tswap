<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_4fe9eb1a243361d47dcab2a32a2d22f34b5bffed09fcf84a04b1281d68f86bcd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd06597c792cb2735a8641611f9a807198749350a413aefdb1cc02e62169a52e = $this->env->getExtension("native_profiler");
        $__internal_dd06597c792cb2735a8641611f9a807198749350a413aefdb1cc02e62169a52e->enter($__internal_dd06597c792cb2735a8641611f9a807198749350a413aefdb1cc02e62169a52e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_dd06597c792cb2735a8641611f9a807198749350a413aefdb1cc02e62169a52e->leave($__internal_dd06597c792cb2735a8641611f9a807198749350a413aefdb1cc02e62169a52e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
