<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_777cafaaf7d0d075f324f5020305b413dd2c5de97c5b3e33fd26d4006d4b2e83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db01604fffae9111daff0b4c59c9936e79391e246569a977a38a674b60c3ed41 = $this->env->getExtension("native_profiler");
        $__internal_db01604fffae9111daff0b4c59c9936e79391e246569a977a38a674b60c3ed41->enter($__internal_db01604fffae9111daff0b4c59c9936e79391e246569a977a38a674b60c3ed41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_db01604fffae9111daff0b4c59c9936e79391e246569a977a38a674b60c3ed41->leave($__internal_db01604fffae9111daff0b4c59c9936e79391e246569a977a38a674b60c3ed41_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
