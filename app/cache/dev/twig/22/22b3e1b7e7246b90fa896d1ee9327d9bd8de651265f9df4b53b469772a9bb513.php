<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_8348b32abe4978742e497ddec2c3c9ad20bdadb0d598a6ad7dcdf7214b4679dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31ca7ad50bcaea499453e61cfc28a689da251e6a0b6bb3eaa135ee36137ea3ce = $this->env->getExtension("native_profiler");
        $__internal_31ca7ad50bcaea499453e61cfc28a689da251e6a0b6bb3eaa135ee36137ea3ce->enter($__internal_31ca7ad50bcaea499453e61cfc28a689da251e6a0b6bb3eaa135ee36137ea3ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_31ca7ad50bcaea499453e61cfc28a689da251e6a0b6bb3eaa135ee36137ea3ce->leave($__internal_31ca7ad50bcaea499453e61cfc28a689da251e6a0b6bb3eaa135ee36137ea3ce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
