<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_4727c405a90bd788d1bd75d7eb35578c36e596e0fb93dfaf4a0fe756f0132565 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_742d18831e675cb1b757064776e257ebf28f1327501c08bdd1f186e98fdd228a = $this->env->getExtension("native_profiler");
        $__internal_742d18831e675cb1b757064776e257ebf28f1327501c08bdd1f186e98fdd228a->enter($__internal_742d18831e675cb1b757064776e257ebf28f1327501c08bdd1f186e98fdd228a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_742d18831e675cb1b757064776e257ebf28f1327501c08bdd1f186e98fdd228a->leave($__internal_742d18831e675cb1b757064776e257ebf28f1327501c08bdd1f186e98fdd228a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
