<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_40df31cd856b474b49adc4923802848a6e42a4e9aea11b4767b8b7a9b56adb48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4137825e1fcfb831e6434a5c2fb06af5f211d65c5b1b2400e413f8be46bf825e = $this->env->getExtension("native_profiler");
        $__internal_4137825e1fcfb831e6434a5c2fb06af5f211d65c5b1b2400e413f8be46bf825e->enter($__internal_4137825e1fcfb831e6434a5c2fb06af5f211d65c5b1b2400e413f8be46bf825e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_4137825e1fcfb831e6434a5c2fb06af5f211d65c5b1b2400e413f8be46bf825e->leave($__internal_4137825e1fcfb831e6434a5c2fb06af5f211d65c5b1b2400e413f8be46bf825e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
