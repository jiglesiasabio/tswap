<?php

/* WebAppBundle:Default:baseTemplate.html.twig */
class __TwigTemplate_55b1839e01a445cd618d8142e5ecc68d6b2679a4d5f7e68a5a39457a0c0b5169 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21e2a69b2e8313b9126e1644e6774dd92d25cfb8dfbe73e4b4b143198a808e47 = $this->env->getExtension("native_profiler");
        $__internal_21e2a69b2e8313b9126e1644e6774dd92d25cfb8dfbe73e4b4b143198a808e47->enter($__internal_21e2a69b2e8313b9126e1644e6774dd92d25cfb8dfbe73e4b4b143198a808e47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:baseTemplate.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>tSwap</title>

    <!-- Bootstrap -->
    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">
    <!-- Custom styles for this template -->
    <link href=\"/css/custom.css\" rel=\"stylesheet\">

    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js\"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
</head>



<body>

<div class=\"container\">
    ";
        // line 30
        $this->loadTemplate("WebAppBundle:Default:partials/header.html.twig", "WebAppBundle:Default:baseTemplate.html.twig", 30)->display(array_merge($context, array(0 => (isset($context["active"]) ? $context["active"] : $this->getContext($context, "active")))));
        // line 31
        echo "
    ";
        // line 32
        $this->displayBlock('content', $context, $blocks);
        // line 35
        echo "
    ";
        // line 36
        $this->loadTemplate("WebAppBundle:Default:partials/footer.html.twig", "WebAppBundle:Default:baseTemplate.html.twig", 36)->display($context);
        // line 37
        echo "
</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src=\"../../assets/js/ie10-viewport-bug-workaround.js\"></script>
</body>



</html>
";
        
        $__internal_21e2a69b2e8313b9126e1644e6774dd92d25cfb8dfbe73e4b4b143198a808e47->leave($__internal_21e2a69b2e8313b9126e1644e6774dd92d25cfb8dfbe73e4b4b143198a808e47_prof);

    }

    // line 32
    public function block_content($context, array $blocks = array())
    {
        $__internal_c6ca23b02945af0ed8ee997e55f830d29eb8010c35573087a99c6f894ac8b0b0 = $this->env->getExtension("native_profiler");
        $__internal_c6ca23b02945af0ed8ee997e55f830d29eb8010c35573087a99c6f894ac8b0b0->enter($__internal_c6ca23b02945af0ed8ee997e55f830d29eb8010c35573087a99c6f894ac8b0b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 33
        echo "
    ";
        
        $__internal_c6ca23b02945af0ed8ee997e55f830d29eb8010c35573087a99c6f894ac8b0b0->leave($__internal_c6ca23b02945af0ed8ee997e55f830d29eb8010c35573087a99c6f894ac8b0b0_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:baseTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 33,  84 => 32,  66 => 37,  64 => 36,  61 => 35,  59 => 32,  56 => 31,  54 => 30,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->*/
/*     <title>tSwap</title>*/
/* */
/*     <!-- Bootstrap -->*/
/*     <link href="/css/bootstrap.min.css" rel="stylesheet">*/
/*     <!-- Custom styles for this template -->*/
/*     <link href="/css/custom.css" rel="stylesheet">*/
/* */
/*     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>*/
/* */
/*     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->*/
/*     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*     <!--[if lt IE 9]>*/
/*     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/*     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/* </head>*/
/* */
/* */
/* */
/* <body>*/
/* */
/* <div class="container">*/
/*     {% include 'WebAppBundle:Default:partials/header.html.twig' with [active] %}*/
/* */
/*     {%  block content %}*/
/* */
/*     {% endblock %}*/
/* */
/*     {%  include 'WebAppBundle:Default:partials/footer.html.twig' %}*/
/* */
/* </div> <!-- /container -->*/
/* */
/* */
/* <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->*/
/* <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>*/
/* </body>*/
/* */
/* */
/* */
/* </html>*/
/* */
