<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_2c7027cbb928324a0ecbca3841e59d67f8cd3ebe7095eaa6fef92f1a0def54bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8e2989df02cd0ec2d0944347f2af72df399b54844f6a445865630bbd8d95ded = $this->env->getExtension("native_profiler");
        $__internal_d8e2989df02cd0ec2d0944347f2af72df399b54844f6a445865630bbd8d95ded->enter($__internal_d8e2989df02cd0ec2d0944347f2af72df399b54844f6a445865630bbd8d95ded_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_d8e2989df02cd0ec2d0944347f2af72df399b54844f6a445865630bbd8d95ded->leave($__internal_d8e2989df02cd0ec2d0944347f2af72df399b54844f6a445865630bbd8d95ded_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
