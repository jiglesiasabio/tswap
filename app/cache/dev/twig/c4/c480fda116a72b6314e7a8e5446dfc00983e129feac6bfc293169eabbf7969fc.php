<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_cd72c3b9bd60f14580581d79504f944ac8a7c8c453659daf7a5742987bebe0c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b80fe6ded65323e8c84fb7c693c4252129d6f98938dec3398ea1742a643bf7d6 = $this->env->getExtension("native_profiler");
        $__internal_b80fe6ded65323e8c84fb7c693c4252129d6f98938dec3398ea1742a643bf7d6->enter($__internal_b80fe6ded65323e8c84fb7c693c4252129d6f98938dec3398ea1742a643bf7d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_b80fe6ded65323e8c84fb7c693c4252129d6f98938dec3398ea1742a643bf7d6->leave($__internal_b80fe6ded65323e8c84fb7c693c4252129d6f98938dec3398ea1742a643bf7d6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
