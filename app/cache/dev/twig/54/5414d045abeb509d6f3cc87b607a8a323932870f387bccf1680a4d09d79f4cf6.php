<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_7da0c8e8ed23aabe2c58772f5919b40163c20bbff5a97564763216a018b93e5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b19c04dbcf61b34f09cad232dab7d2328bc2acacacf3dcc7585bb4dd7da4232 = $this->env->getExtension("native_profiler");
        $__internal_7b19c04dbcf61b34f09cad232dab7d2328bc2acacacf3dcc7585bb4dd7da4232->enter($__internal_7b19c04dbcf61b34f09cad232dab7d2328bc2acacacf3dcc7585bb4dd7da4232_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"
<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_7b19c04dbcf61b34f09cad232dab7d2328bc2acacacf3dcc7585bb4dd7da4232->leave($__internal_7b19c04dbcf61b34f09cad232dab7d2328bc2acacacf3dcc7585bb4dd7da4232_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* id="<?php echo $view->escape($id) ?>" name="<?php echo $view->escape($full_name) ?>"*/
/* <?php if ($disabled): ?>disabled="disabled" <?php endif ?>*/
/* <?php foreach ($choice_attr as $k => $v): ?>*/
/* <?php if ($v === true): ?>*/
/* <?php printf('%s="%s" ', $view->escape($k), $view->escape($k)) ?>*/
/* <?php elseif ($v !== false): ?>*/
/* <?php printf('%s="%s" ', $view->escape($k), $view->escape($v)) ?>*/
/* <?php endif ?>*/
/* <?php endforeach ?>*/
/* */
