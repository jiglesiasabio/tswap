<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_234e6237ea7f45e3ce73ebd275d4496a8dbe9b957002e23e8e687aba6fd9e893 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c18e8069d5ca4d334efea62082ffc38ebc686124aa685d3f7ecf5162a6ede31b = $this->env->getExtension("native_profiler");
        $__internal_c18e8069d5ca4d334efea62082ffc38ebc686124aa685d3f7ecf5162a6ede31b->enter($__internal_c18e8069d5ca4d334efea62082ffc38ebc686124aa685d3f7ecf5162a6ede31b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_c18e8069d5ca4d334efea62082ffc38ebc686124aa685d3f7ecf5162a6ede31b->leave($__internal_c18e8069d5ca4d334efea62082ffc38ebc686124aa685d3f7ecf5162a6ede31b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
