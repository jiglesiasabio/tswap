<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_1f1f076a434ec33cd3c95534e72bc9a3e61d65188e5860e478e71f278e069a91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28378c107590b4b571378a49968c3f0922fa1d88f20f50dec23843bc1e631bab = $this->env->getExtension("native_profiler");
        $__internal_28378c107590b4b571378a49968c3f0922fa1d88f20f50dec23843bc1e631bab->enter($__internal_28378c107590b4b571378a49968c3f0922fa1d88f20f50dec23843bc1e631bab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_28378c107590b4b571378a49968c3f0922fa1d88f20f50dec23843bc1e631bab->leave($__internal_28378c107590b4b571378a49968c3f0922fa1d88f20f50dec23843bc1e631bab_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
