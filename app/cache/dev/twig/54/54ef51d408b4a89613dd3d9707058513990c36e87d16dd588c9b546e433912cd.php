<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_f48ce63d0a9dd93ec9995ea805cf427c4e8a2065bf88aa8d63a565e29b30ff12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45dd484805c7019ca7ff934aedac8e7e5b5d3130ba512318477620b9c03ef670 = $this->env->getExtension("native_profiler");
        $__internal_45dd484805c7019ca7ff934aedac8e7e5b5d3130ba512318477620b9c03ef670->enter($__internal_45dd484805c7019ca7ff934aedac8e7e5b5d3130ba512318477620b9c03ef670_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_45dd484805c7019ca7ff934aedac8e7e5b5d3130ba512318477620b9c03ef670->leave($__internal_45dd484805c7019ca7ff934aedac8e7e5b5d3130ba512318477620b9c03ef670_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
