<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 00:32
 */

namespace TicketListingBundle\Services;


use TicketListingBundle\Entity\User;
use TicketListingBundle\Services\DAO\UserDAO;

class UserService
{
    private $userDAO;

    /**
     * UserService constructor.
     * @param UserDAO $userDAO
     */
    public function __construct(UserDAO $userDAO)
    {
        $this->userDAO = $userDAO;
    }

    /**
     * @param $name
     */
    public function createNewUser($name)
    {
        // Here we will be validating input when it gets more complicated (emails, etc.) w/ a User Validator.
        $user = new User();
        $user->setName($name);

        $this->userDAO->save($user);
        //Here we will be dispatching an eventual 'userCreated' event for tracking, sending emails...

        return $user;
    }
}