<?php

/* WebAppBundle:Default:partials/header.html.twig */
class __TwigTemplate_f67e90a9c2affb7a255f398d9b048e5f25150ff5c476a89e04ab30eaaa97479d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4293a4568d229e0cd0a4778a124870e60cbf59cdec6c3d59317c7949f18cc2d0 = $this->env->getExtension("native_profiler");
        $__internal_4293a4568d229e0cd0a4778a124870e60cbf59cdec6c3d59317c7949f18cc2d0->enter($__internal_4293a4568d229e0cd0a4778a124870e60cbf59cdec6c3d59317c7949f18cc2d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:partials/header.html.twig"));

        // line 1
        echo "<nav class=\"navbar navbar-default navbar-fixed-top\">
    <a class=\"navbar-brand\" href=\"";
        // line 2
        echo $this->env->getExtension('routing')->getPath("web_app_index");
        echo "\">tSwap</a>

    <ul class=\"nav navbar-nav\">
        <li ";
        // line 5
        if (((isset($context["active"]) ? $context["active"] : $this->getContext($context, "active")) == "users")) {
            echo "class=\"active\"";
        }
        echo ">
            <a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("web_app_users");
        echo "\">Users API</a>
        </li>
        <li ";
        // line 8
        if (((isset($context["active"]) ? $context["active"] : $this->getContext($context, "active")) == "listings")) {
            echo "class=\"active\"";
        }
        echo ">
            <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("web_app_listings");
        echo "\">Listings API</a>
        </li>
        <li ";
        // line 11
        if (((isset($context["active"]) ? $context["active"] : $this->getContext($context, "active")) == "purchase")) {
            echo "class=\"active\"";
        }
        echo ">
            <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("web_app_purchase");
        echo "\">Purchase API</a>
        </li>

        <li ";
        // line 15
        if (((isset($context["active"]) ? $context["active"] : $this->getContext($context, "active")) == "userList")) {
            echo "class=\"active\"";
        }
        echo ">
            <a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("web_app_users_list");
        echo "\">Users List</a>
        </li>

        <li ";
        // line 19
        if (((isset($context["active"]) ? $context["active"] : $this->getContext($context, "active")) == "listingsList")) {
            echo "class=\"active\"";
        }
        echo ">
            <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("web_app_listings_list");
        echo "\">Listings List</a>
        </li>

    </ul>
</nav>";
        
        $__internal_4293a4568d229e0cd0a4778a124870e60cbf59cdec6c3d59317c7949f18cc2d0->leave($__internal_4293a4568d229e0cd0a4778a124870e60cbf59cdec6c3d59317c7949f18cc2d0_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:partials/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 20,  77 => 19,  71 => 16,  65 => 15,  59 => 12,  53 => 11,  48 => 9,  42 => 8,  37 => 6,  31 => 5,  25 => 2,  22 => 1,);
    }
}
/* <nav class="navbar navbar-default navbar-fixed-top">*/
/*     <a class="navbar-brand" href="{{ path('web_app_index') }}">tSwap</a>*/
/* */
/*     <ul class="nav navbar-nav">*/
/*         <li {% if (active == 'users') %}class="active"{% endif %}>*/
/*             <a href="{{ path('web_app_users') }}">Users API</a>*/
/*         </li>*/
/*         <li {% if (active == 'listings') %}class="active"{% endif %}>*/
/*             <a href="{{ path('web_app_listings') }}">Listings API</a>*/
/*         </li>*/
/*         <li {% if (active == 'purchase') %}class="active"{% endif %}>*/
/*             <a href="{{ path('web_app_purchase') }}">Purchase API</a>*/
/*         </li>*/
/* */
/*         <li {% if (active == 'userList') %}class="active"{% endif %}>*/
/*             <a href="{{ path('web_app_users_list') }}">Users List</a>*/
/*         </li>*/
/* */
/*         <li {% if (active == 'listingsList') %}class="active"{% endif %}>*/
/*             <a href="{{ path('web_app_listings_list') }}">Listings List</a>*/
/*         </li>*/
/* */
/*     </ul>*/
/* </nav>*/
