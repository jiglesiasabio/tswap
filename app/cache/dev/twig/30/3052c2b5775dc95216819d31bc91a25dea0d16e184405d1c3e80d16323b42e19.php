<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_f5e707a8bba55a713a4430e56168c53f6243ca3a2b30a9930a5af2d34879ae95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e612f0bb7062c55e1ada687776fc40e67aef505b960f8761c9753112c352fb4a = $this->env->getExtension("native_profiler");
        $__internal_e612f0bb7062c55e1ada687776fc40e67aef505b960f8761c9753112c352fb4a->enter($__internal_e612f0bb7062c55e1ada687776fc40e67aef505b960f8761c9753112c352fb4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_e612f0bb7062c55e1ada687776fc40e67aef505b960f8761c9753112c352fb4a->leave($__internal_e612f0bb7062c55e1ada687776fc40e67aef505b960f8761c9753112c352fb4a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
