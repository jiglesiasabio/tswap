<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_aacd23a894e25834a2369aad5e2418fe060d507c820a180e21a73f1b90c1c4f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_534af227ad47c55f02510d6fc093ec0d5c6862db8ab0cceb4a4e89384a65785c = $this->env->getExtension("native_profiler");
        $__internal_534af227ad47c55f02510d6fc093ec0d5c6862db8ab0cceb4a4e89384a65785c->enter($__internal_534af227ad47c55f02510d6fc093ec0d5c6862db8ab0cceb4a4e89384a65785c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_534af227ad47c55f02510d6fc093ec0d5c6862db8ab0cceb4a4e89384a65785c->leave($__internal_534af227ad47c55f02510d6fc093ec0d5c6862db8ab0cceb4a4e89384a65785c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
