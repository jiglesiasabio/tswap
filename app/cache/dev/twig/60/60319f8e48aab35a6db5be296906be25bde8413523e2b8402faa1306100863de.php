<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_f0c2c00d4da3c06eff4b64e37d3f59ea45d25ca24ee4dd238393834c2226b8c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f443642d12c86ca30ec15039a93fde79384180de53531101fe778ef4c73e9b41 = $this->env->getExtension("native_profiler");
        $__internal_f443642d12c86ca30ec15039a93fde79384180de53531101fe778ef4c73e9b41->enter($__internal_f443642d12c86ca30ec15039a93fde79384180de53531101fe778ef4c73e9b41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_f443642d12c86ca30ec15039a93fde79384180de53531101fe778ef4c73e9b41->leave($__internal_f443642d12c86ca30ec15039a93fde79384180de53531101fe778ef4c73e9b41_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
