<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_74d936e684ffb8084e081d2f6f1aa0a725c6cae62d1bfcb33c2c06805fec8df1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe4ce68221a7dda07e5e3e9b2bb3181ed96e74ba0ba7da464181db02c06c865f = $this->env->getExtension("native_profiler");
        $__internal_fe4ce68221a7dda07e5e3e9b2bb3181ed96e74ba0ba7da464181db02c06c865f->enter($__internal_fe4ce68221a7dda07e5e3e9b2bb3181ed96e74ba0ba7da464181db02c06c865f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_fe4ce68221a7dda07e5e3e9b2bb3181ed96e74ba0ba7da464181db02c06c865f->leave($__internal_fe4ce68221a7dda07e5e3e9b2bb3181ed96e74ba0ba7da464181db02c06c865f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
