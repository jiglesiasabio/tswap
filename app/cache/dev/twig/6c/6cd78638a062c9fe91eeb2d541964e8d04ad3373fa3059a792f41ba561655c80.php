<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_c983fdf0145eabd5a0d9838f63cf51930b167133acac060548f8297c555b8bca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15b753d0bc6058784d809e3a8cde69f4876c43b62a3c00b2d8064832b832b3a1 = $this->env->getExtension("native_profiler");
        $__internal_15b753d0bc6058784d809e3a8cde69f4876c43b62a3c00b2d8064832b832b3a1->enter($__internal_15b753d0bc6058784d809e3a8cde69f4876c43b62a3c00b2d8064832b832b3a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_15b753d0bc6058784d809e3a8cde69f4876c43b62a3c00b2d8064832b832b3a1->leave($__internal_15b753d0bc6058784d809e3a8cde69f4876c43b62a3c00b2d8064832b832b3a1_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_01a372395c1fa7d51e3eeb6afab3ff7d561aa5a9894ff9810890af6cc7819211 = $this->env->getExtension("native_profiler");
        $__internal_01a372395c1fa7d51e3eeb6afab3ff7d561aa5a9894ff9810890af6cc7819211->enter($__internal_01a372395c1fa7d51e3eeb6afab3ff7d561aa5a9894ff9810890af6cc7819211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_01a372395c1fa7d51e3eeb6afab3ff7d561aa5a9894ff9810890af6cc7819211->leave($__internal_01a372395c1fa7d51e3eeb6afab3ff7d561aa5a9894ff9810890af6cc7819211_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_43faf343bb51cb7bd3b9091bc0d3440bd83255d3329e8eba2e7db6c373ad2a8a = $this->env->getExtension("native_profiler");
        $__internal_43faf343bb51cb7bd3b9091bc0d3440bd83255d3329e8eba2e7db6c373ad2a8a->enter($__internal_43faf343bb51cb7bd3b9091bc0d3440bd83255d3329e8eba2e7db6c373ad2a8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_43faf343bb51cb7bd3b9091bc0d3440bd83255d3329e8eba2e7db6c373ad2a8a->leave($__internal_43faf343bb51cb7bd3b9091bc0d3440bd83255d3329e8eba2e7db6c373ad2a8a_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_4f36f3370321d87b95e632aeff5558febf75788c2e608f03ba4ace66794ff3cd = $this->env->getExtension("native_profiler");
        $__internal_4f36f3370321d87b95e632aeff5558febf75788c2e608f03ba4ace66794ff3cd->enter($__internal_4f36f3370321d87b95e632aeff5558febf75788c2e608f03ba4ace66794ff3cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_4f36f3370321d87b95e632aeff5558febf75788c2e608f03ba4ace66794ff3cd->leave($__internal_4f36f3370321d87b95e632aeff5558febf75788c2e608f03ba4ace66794ff3cd_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
