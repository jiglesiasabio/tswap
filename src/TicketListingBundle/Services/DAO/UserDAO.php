<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 00:31
 */

namespace TicketListingBundle\Services\DAO;


use Doctrine\ORM\EntityManager;
use TicketListingBundle\Entity\User;

class UserDAO extends AbstractDAO
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(User::class);
    }
}