<?php

/* WebAppBundle:Default:index.html.twig */
class __TwigTemplate_d94b19f52b79afe12efa20a6eb555b778c58b19f0da9937dee84c5ae9dfd3622 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:baseTemplate.html.twig", "WebAppBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:baseTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72d1023f2b43508cdd641b698d26804c6481125ea220b68d76ba0ff1da78cb36 = $this->env->getExtension("native_profiler");
        $__internal_72d1023f2b43508cdd641b698d26804c6481125ea220b68d76ba0ff1da78cb36->enter($__internal_72d1023f2b43508cdd641b698d26804c6481125ea220b68d76ba0ff1da78cb36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_72d1023f2b43508cdd641b698d26804c6481125ea220b68d76ba0ff1da78cb36->leave($__internal_72d1023f2b43508cdd641b698d26804c6481125ea220b68d76ba0ff1da78cb36_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_10fd4c976380475453fa99be2c1a46162e6194b26d11546da96c0643b381fdab = $this->env->getExtension("native_profiler");
        $__internal_10fd4c976380475453fa99be2c1a46162e6194b26d11546da96c0643b381fdab->enter($__internal_10fd4c976380475453fa99be2c1a46162e6194b26d11546da96c0643b381fdab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"row marketing\">
        <div class=\"col-lg-12\">
            <h1>tSwap test area</h1>
            <p class=\"lead\">Welcome to the test area, here you will be able to test some endpoints of our API.</p>

            <hr>

            <h3>What can you do here?</h3>

            <h4>Create Users</h4>
            <p>You can create a user to interact with the API.</p>

            <h4>Create Listings</h4>
            <p>You can create a listing to sell your tickets specifying their barcodes.</p>

            <h4>Buy Listings</h4>
            <p>If you see a listing you like (and have some virtual money!) you can buy it!.</p>

            <h4>List Listings</h4>
            <p>Just for browsing.</p>

            <h4>List Users</h4>
            <p>Just to see if who is around.</p>
        </div>
    </div>
";
        
        $__internal_10fd4c976380475453fa99be2c1a46162e6194b26d11546da96c0643b381fdab->leave($__internal_10fd4c976380475453fa99be2c1a46162e6194b26d11546da96c0643b381fdab_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:baseTemplate.html.twig' %}*/
/* */
/* {%  block content %}*/
/*     <div class="row marketing">*/
/*         <div class="col-lg-12">*/
/*             <h1>tSwap test area</h1>*/
/*             <p class="lead">Welcome to the test area, here you will be able to test some endpoints of our API.</p>*/
/* */
/*             <hr>*/
/* */
/*             <h3>What can you do here?</h3>*/
/* */
/*             <h4>Create Users</h4>*/
/*             <p>You can create a user to interact with the API.</p>*/
/* */
/*             <h4>Create Listings</h4>*/
/*             <p>You can create a listing to sell your tickets specifying their barcodes.</p>*/
/* */
/*             <h4>Buy Listings</h4>*/
/*             <p>If you see a listing you like (and have some virtual money!) you can buy it!.</p>*/
/* */
/*             <h4>List Listings</h4>*/
/*             <p>Just for browsing.</p>*/
/* */
/*             <h4>List Users</h4>*/
/*             <p>Just to see if who is around.</p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
