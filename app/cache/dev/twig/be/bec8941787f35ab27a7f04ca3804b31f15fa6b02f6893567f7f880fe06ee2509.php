<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_8d3dcaa94fc75b05a65446586666bae5fc195804fb99aeadb669aba05164432e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12207710cddd0c48dacb7a1334d26ab07c23bb664ca95a796d391c601d5c4429 = $this->env->getExtension("native_profiler");
        $__internal_12207710cddd0c48dacb7a1334d26ab07c23bb664ca95a796d391c601d5c4429->enter($__internal_12207710cddd0c48dacb7a1334d26ab07c23bb664ca95a796d391c601d5c4429_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_12207710cddd0c48dacb7a1334d26ab07c23bb664ca95a796d391c601d5c4429->leave($__internal_12207710cddd0c48dacb7a1334d26ab07c23bb664ca95a796d391c601d5c4429_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
