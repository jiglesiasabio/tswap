<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 06/07/16
 * Time: 23:54
 */

namespace TicketListingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * A barcode can only appear once associated to a given ticket
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="barcode_ticket_unique", columns={"ticket_id", "barcode"})})
 *
 * @ORM\Entity()
 */
class Barcode implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ticket", inversedBy="barcodes")
     */
    protected $ticket;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    protected $barcode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Barcode
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param mixed $barcode
     * @return Barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param mixed $ticket
     * @return Barcode
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
        return $this;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'barcode' => $this->getBarcode()
        ];
    }




}