<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_ac40a390cd9713d5d638e3afa7e9f9786e5def9d7bb8c743ced3d058f678d2e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13d97146ebfa4fb6519895a482c89f21a52e35b1a0cc00018d533970d50f15d3 = $this->env->getExtension("native_profiler");
        $__internal_13d97146ebfa4fb6519895a482c89f21a52e35b1a0cc00018d533970d50f15d3->enter($__internal_13d97146ebfa4fb6519895a482c89f21a52e35b1a0cc00018d533970d50f15d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_13d97146ebfa4fb6519895a482c89f21a52e35b1a0cc00018d533970d50f15d3->leave($__internal_13d97146ebfa4fb6519895a482c89f21a52e35b1a0cc00018d533970d50f15d3_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_70c38d0c7deebec4a603bc53f785ee093dac571cf18e3b2016ac2b1fd31fabc1 = $this->env->getExtension("native_profiler");
        $__internal_70c38d0c7deebec4a603bc53f785ee093dac571cf18e3b2016ac2b1fd31fabc1->enter($__internal_70c38d0c7deebec4a603bc53f785ee093dac571cf18e3b2016ac2b1fd31fabc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_70c38d0c7deebec4a603bc53f785ee093dac571cf18e3b2016ac2b1fd31fabc1->leave($__internal_70c38d0c7deebec4a603bc53f785ee093dac571cf18e3b2016ac2b1fd31fabc1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
