<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 00:37
 */

namespace TicketListingBundle\Controller;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TicketListingBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as API;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use TicketListingBundle\Services\UserService;


/**
 * @Route("/user")
 */
class UserController extends BaseController
{
    /**
     * Endpoint for creating a new User.
     *
     * @API\Post("", name="user_post")
     * @API\Post("/", name="user_post_slashed")
     *
     * @API\RequestParam(name="name", nullable=false)
     */
    public function createUserAction(Request $request)
    {
        /** @var UserService $userDAO */
        $userDAO = $this->get('ticket_listing.user.service');

        try {
            $user = $userDAO->createNewUser($request->request->get('name'));
        } catch (UniqueConstraintViolationException $e) {
            return $this->errorResponse("User name already used.");
        }

        return $this->successResponse($user);
    }

    /**
     * Endpoint for fetching the profile of an existing User.
     *
     * @API\Get("/{id}", name="user_get")
     */
    public function getUserAction(User $user)
    {
        $responseBody = [
            "status" => "success",
            "data" => $user
        ];
        return new JsonResponse($responseBody, 200);
    }

}