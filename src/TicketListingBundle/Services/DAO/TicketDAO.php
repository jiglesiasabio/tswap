<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 09:06
 */

namespace TicketListingBundle\Services\DAO;


use Doctrine\ORM\EntityManager;
use TicketListingBundle\Entity\Listing;

class TicketDAO extends AbstractDAO
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Listing::class);
    }

}