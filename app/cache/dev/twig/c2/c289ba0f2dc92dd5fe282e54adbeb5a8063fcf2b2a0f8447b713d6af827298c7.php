<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_2c7ea424849419a1784a5fe6412b8808f70a4382305f8aaff41e78ff9970ce9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf5c5daaed180aa4f8b3ac8d1d7e60e99181512fdef2dc90f14716163157297c = $this->env->getExtension("native_profiler");
        $__internal_cf5c5daaed180aa4f8b3ac8d1d7e60e99181512fdef2dc90f14716163157297c->enter($__internal_cf5c5daaed180aa4f8b3ac8d1d7e60e99181512fdef2dc90f14716163157297c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_cf5c5daaed180aa4f8b3ac8d1d7e60e99181512fdef2dc90f14716163157297c->leave($__internal_cf5c5daaed180aa4f8b3ac8d1d7e60e99181512fdef2dc90f14716163157297c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
