<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_f480919a36f8cc093b7fc885da9f57b8c0bee7f1b2614f64c2c5a8085dd3b248 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1aaa916d1c8b2846152007dbea8247c382bb3a5d90443f2e66559445fce1ea5d = $this->env->getExtension("native_profiler");
        $__internal_1aaa916d1c8b2846152007dbea8247c382bb3a5d90443f2e66559445fce1ea5d->enter($__internal_1aaa916d1c8b2846152007dbea8247c382bb3a5d90443f2e66559445fce1ea5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1aaa916d1c8b2846152007dbea8247c382bb3a5d90443f2e66559445fce1ea5d->leave($__internal_1aaa916d1c8b2846152007dbea8247c382bb3a5d90443f2e66559445fce1ea5d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_2947d26fb75b704f5557905de5a51e6c75562374e47f44a76190c17a76f6349d = $this->env->getExtension("native_profiler");
        $__internal_2947d26fb75b704f5557905de5a51e6c75562374e47f44a76190c17a76f6349d->enter($__internal_2947d26fb75b704f5557905de5a51e6c75562374e47f44a76190c17a76f6349d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_2947d26fb75b704f5557905de5a51e6c75562374e47f44a76190c17a76f6349d->leave($__internal_2947d26fb75b704f5557905de5a51e6c75562374e47f44a76190c17a76f6349d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_890e32067f744ab6085484f2b562ba04d2f3a6d91a9c915760c7585fd6a79fad = $this->env->getExtension("native_profiler");
        $__internal_890e32067f744ab6085484f2b562ba04d2f3a6d91a9c915760c7585fd6a79fad->enter($__internal_890e32067f744ab6085484f2b562ba04d2f3a6d91a9c915760c7585fd6a79fad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_890e32067f744ab6085484f2b562ba04d2f3a6d91a9c915760c7585fd6a79fad->leave($__internal_890e32067f744ab6085484f2b562ba04d2f3a6d91a9c915760c7585fd6a79fad_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
