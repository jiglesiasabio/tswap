<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 09:04
 */

namespace TicketListingBundle\Services\DAO;


use Doctrine\ORM\EntityManager;
use TicketListingBundle\Entity\Barcode;

class BarcodeDAO extends AbstractDAO
{

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Barcode::class);
    }

    private function buildInQueryFromBarcodeList($barcodes)
    {
        $quotedBarcodes = [];
        foreach ($barcodes as $bc) {
            $quotedBarcodes[] = '"' . $bc . '"';
        }
        $inQuery = implode(', ', $quotedBarcodes);
        return $inQuery;
    }

    /**
     * Receives a list of barcodes (EAN-XXX).
     * Returns true if one (or more) of them are currently listed in non-bought tickets false otherwise.
     *
     * @param array $barcodes
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function checkIfBarcodesAreOnActiveListings(array $barcodes)
    {
        $connection = $this->entityManager->getConnection();

        $inQuery = $this->buildInQueryFromBarcodeList($barcodes);

        $statement = $connection->prepare("SELECT bc.*
# All the barcodes linked to a listed ticket
FROM tswap.barcode AS bc
JOIN tswap.ticket AS tk ON bc.ticket_id = tk.id
JOIN tswap.listing AS lt ON tk.listing_id = lt.id
#not bought
WHERE tk.bought_at IS null
# In the list of
AND bc.barcode in ($inQuery);");
        $statement->execute();
        $results = $statement->fetchAll();

        return (bool)count($results);
    }

    /**
     * Given a list of barcodes (EAN-XXX) returns a list with the ids of the last buyers (if any)
     *
     * @param $barcodeId
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getUserIdOfBarcodeAssociatedLastBuyer($barcodes)
    {
        $connection = $this->entityManager->getConnection();

        $inQuery = $this->buildInQueryFromBarcodeList($barcodes);

        $statement = $connection->prepare("SELECT tk.buyer_user_id FROM tswap.barcode AS bc
JOIN tswap.ticket AS tk
ON tk.id = bc.ticket_id
WHERE tk.bought_at IS NOT NULL
AND bc.barcode IN ($inQuery)
ORDER BY tk.bought_at DESC
LIMIT 1;");
        $statement->execute();
        $results = $statement->fetchAll();

        $buyersIds = [];
        foreach ($results as $row) {
            $buyersIds[] = $row['buyer_user_id'];
        }

        return $buyersIds;
    }

}