<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_31f420249a771ee74499b4ecf2060302c01c14c1bb362d2beb6059d2609a0ae8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_998176b36bf74b4eab646f21afa7aa8dc40a36e79115157ed25275da196e2921 = $this->env->getExtension("native_profiler");
        $__internal_998176b36bf74b4eab646f21afa7aa8dc40a36e79115157ed25275da196e2921->enter($__internal_998176b36bf74b4eab646f21afa7aa8dc40a36e79115157ed25275da196e2921_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_998176b36bf74b4eab646f21afa7aa8dc40a36e79115157ed25275da196e2921->leave($__internal_998176b36bf74b4eab646f21afa7aa8dc40a36e79115157ed25275da196e2921_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
