<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_593ada752204ad4a89492c001ce227be4193e053c43f8f6d05fc89bf8e8a6c18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb931aa54712d0abe9ecd1f4b4840cb80cd4199c4f6a5e87261cf9f5be88c031 = $this->env->getExtension("native_profiler");
        $__internal_fb931aa54712d0abe9ecd1f4b4840cb80cd4199c4f6a5e87261cf9f5be88c031->enter($__internal_fb931aa54712d0abe9ecd1f4b4840cb80cd4199c4f6a5e87261cf9f5be88c031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_fb931aa54712d0abe9ecd1f4b4840cb80cd4199c4f6a5e87261cf9f5be88c031->leave($__internal_fb931aa54712d0abe9ecd1f4b4840cb80cd4199c4f6a5e87261cf9f5be88c031_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
