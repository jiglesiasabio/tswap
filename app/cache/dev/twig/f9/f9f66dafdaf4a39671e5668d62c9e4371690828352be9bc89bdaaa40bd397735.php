<?php

/* WebAppBundle:Default:listingPage.html.twig */
class __TwigTemplate_1e0d18d7150202402ff341542050ccef59bc5597176e862ee119b979a66e533a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:baseTemplate.html.twig", "WebAppBundle:Default:listingPage.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'description' => array($this, 'block_description'),
            'list' => array($this, 'block_list'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:baseTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd02ed286f22c2de48085a846b6e4c7801ed698f7f65422c149ef1219e441eff = $this->env->getExtension("native_profiler");
        $__internal_bd02ed286f22c2de48085a846b6e4c7801ed698f7f65422c149ef1219e441eff->enter($__internal_bd02ed286f22c2de48085a846b6e4c7801ed698f7f65422c149ef1219e441eff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:listingPage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd02ed286f22c2de48085a846b6e4c7801ed698f7f65422c149ef1219e441eff->leave($__internal_bd02ed286f22c2de48085a846b6e4c7801ed698f7f65422c149ef1219e441eff_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_968802c35a70ffbcb16f17942072b44f67ac3461145eac5eaaf68c9f64716229 = $this->env->getExtension("native_profiler");
        $__internal_968802c35a70ffbcb16f17942072b44f67ac3461145eac5eaaf68c9f64716229->enter($__internal_968802c35a70ffbcb16f17942072b44f67ac3461145eac5eaaf68c9f64716229_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"row marketing\">
        <div class=\"col-lg-12\">

            ";
        // line 7
        $this->displayBlock('description', $context, $blocks);
        // line 9
        echo "
            <hr>

            ";
        // line 12
        $this->displayBlock('list', $context, $blocks);
        // line 14
        echo "    </div>

";
        
        $__internal_968802c35a70ffbcb16f17942072b44f67ac3461145eac5eaaf68c9f64716229->leave($__internal_968802c35a70ffbcb16f17942072b44f67ac3461145eac5eaaf68c9f64716229_prof);

    }

    // line 7
    public function block_description($context, array $blocks = array())
    {
        $__internal_bee93218b0507537771d04b96db8549a3d908e8231488c6b48893699e3d65ee7 = $this->env->getExtension("native_profiler");
        $__internal_bee93218b0507537771d04b96db8549a3d908e8231488c6b48893699e3d65ee7->enter($__internal_bee93218b0507537771d04b96db8549a3d908e8231488c6b48893699e3d65ee7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 8
        echo "            ";
        
        $__internal_bee93218b0507537771d04b96db8549a3d908e8231488c6b48893699e3d65ee7->leave($__internal_bee93218b0507537771d04b96db8549a3d908e8231488c6b48893699e3d65ee7_prof);

    }

    // line 12
    public function block_list($context, array $blocks = array())
    {
        $__internal_2e0ec9df2985ee8381af401ef70a3acb21dcfb2be3bdd6ffc130baf1baeb5017 = $this->env->getExtension("native_profiler");
        $__internal_2e0ec9df2985ee8381af401ef70a3acb21dcfb2be3bdd6ffc130baf1baeb5017->enter($__internal_2e0ec9df2985ee8381af401ef70a3acb21dcfb2be3bdd6ffc130baf1baeb5017_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        // line 13
        echo "            ";
        
        $__internal_2e0ec9df2985ee8381af401ef70a3acb21dcfb2be3bdd6ffc130baf1baeb5017->leave($__internal_2e0ec9df2985ee8381af401ef70a3acb21dcfb2be3bdd6ffc130baf1baeb5017_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:listingPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 13,  78 => 12,  71 => 8,  65 => 7,  56 => 14,  54 => 12,  49 => 9,  47 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:baseTemplate.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <div class="row marketing">*/
/*         <div class="col-lg-12">*/
/* */
/*             {% block description %}*/
/*             {% endblock %}*/
/* */
/*             <hr>*/
/* */
/*             {% block list %}*/
/*             {% endblock %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
