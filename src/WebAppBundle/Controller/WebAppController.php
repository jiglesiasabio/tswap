<?php

namespace WebAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use TicketListingBundle\Services\DAO\ListingDAO;
use TicketListingBundle\Services\DAO\UserDAO;

/**
 * @Route("/app")
 */
class WebAppController extends Controller
{
    /**
     * @Route("/", name="web_app_index")
     */
    public function indexAction()
    {
        $data = ['active' => 'index'];
        return $this->render('WebAppBundle:Default:index.html.twig', $data);
    }

    /**
     * @Route("/users", name="web_app_users")
     */
    public function usersAction()
    {
        $data = ['active' => 'users'];
        return $this->render('WebAppBundle:Default:users.html.twig', $data);
    }

    /**
     * @Route("/listings", name="web_app_listings")
     */
    public function listingsAction()
    {
        $data = ['active' => 'listings'];
        return $this->render('WebAppBundle:Default:listings.html.twig', $data);
    }

    /**
     * @Route("/purchase", name="web_app_purchase")
     */
    public function purchaseAction()
    {
        $data = ['active' => 'purchase'];
        return $this->render('WebAppBundle:Default:purchase.html.twig', $data);
    }

    /**
     * @Route("/users_list", name="web_app_users_list")
     */
    public function usersListAction()
    {
        /** @var UserDAO $userDAO */
        $userDAO = $this->get('ticket_listing.user.dao');

        $data = [
            'active' => 'users_list',
            'users' => $userDAO->findAll()
        ];
        return $this->render('WebAppBundle:Default:usersList.html.twig', $data);
    }

    /**
     * @Route("/listings_list", name="web_app_listings_list")
     */
    public function listingsListAction()
    {

        /** @var ListingDAO $listingsDAO */
        $listingsDAO = $this->get('ticket_listing.listing.dao');

        $data = [
            'active' => 'listings_list',
            'listings' => $listingsDAO->findAll()
        ];

        return $this->render('WebAppBundle:Default:listingsList.html.twig', $data);
    }
}
