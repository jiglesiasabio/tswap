<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_ec63d11fd2c2896bb5df962cc2b51152ceeec572563c119d60319a36e426bd0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc2d24dba4c568eeb0052635adb7b9b0ed6955ea623030b30f8fcfd25e3fef72 = $this->env->getExtension("native_profiler");
        $__internal_bc2d24dba4c568eeb0052635adb7b9b0ed6955ea623030b30f8fcfd25e3fef72->enter($__internal_bc2d24dba4c568eeb0052635adb7b9b0ed6955ea623030b30f8fcfd25e3fef72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_bc2d24dba4c568eeb0052635adb7b9b0ed6955ea623030b30f8fcfd25e3fef72->leave($__internal_bc2d24dba4c568eeb0052635adb7b9b0ed6955ea623030b30f8fcfd25e3fef72_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
