<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_330e1f33a2edee74dab9a39c45dfa257c570cc6aeefc5a87466afaa9ce577ca4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7828673a1ac1142456fcfb2455b3b9245a50337dfc3ed6b24f827f691066d461 = $this->env->getExtension("native_profiler");
        $__internal_7828673a1ac1142456fcfb2455b3b9245a50337dfc3ed6b24f827f691066d461->enter($__internal_7828673a1ac1142456fcfb2455b3b9245a50337dfc3ed6b24f827f691066d461_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_7828673a1ac1142456fcfb2455b3b9245a50337dfc3ed6b24f827f691066d461->leave($__internal_7828673a1ac1142456fcfb2455b3b9245a50337dfc3ed6b24f827f691066d461_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
