<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 10:39
 */

namespace TicketListingBundle\Services;


use Symfony\Component\HttpKernel\Exception\HttpException;
use TicketListingBundle\Entity\Barcode;
use TicketListingBundle\Entity\Listing;
use TicketListingBundle\Entity\Ticket;
use TicketListingBundle\Entity\User;
use TicketListingBundle\Services\DAO\BarcodeDAO;
use TicketListingBundle\Services\DAO\ListingDAO;

class ListingService
{
    /** @var ListingDAO */
    private $listingDAO;
    /** @var  BarcodeDAO */
    private $barcodeDAO;

    /**
     * ListingService constructor.
     * @param $listingDAO
     */
    public function __construct(ListingDAO $listingDAO, BarcodeDAO $barcodeDAO)
    {
        $this->listingDAO = $listingDAO;
        $this->barcodeDAO = $barcodeDAO;
    }

    /**
     * Receives a list of tickets (as array) and returns a list with all the barcodes used.
     *
     * @param $tickets
     * @return array
     */
    private function getBarcodesList($tickets)
    {
        $barcodes = [];

        foreach ($tickets as $ticket) {
            if (array_key_exists('barcodes', $ticket)) {
                foreach ($ticket['barcodes'] as $barcode) {
                    $barcodes[] = $barcode;
                }
            }
        }
        return $barcodes;
    }

    /**
     * Manages the whole flow for creating a Listing with all the tickets and their associated barcodes
     * and persisting it after validating all the business rules.
     *
     * @param User $user
     * @param $sellingPrice
     * @param $description
     * @param $tickets
     * @return Listing
     */
    public function createListing(User $user, $sellingPrice, $description, $tickets)
    {
        // Process the restrictions that apply to the barcodes
        $barcodes = $this->getBarcodesList($tickets);

        // Business rules for Listing insertion
        $this->checkForDuplicatedBarcodes($barcodes);
        $this->checkForBarcodesInActiveListings($barcodes);
        $this->checkIfUserCanBuyedPreviouslyListedBarcodes($user, $barcodes);

        // If we're still here we can create the listing
        $listing = $this->createNewListing($user, $sellingPrice, $description, $tickets);

        // Persist (linked entitites will cascade)
        $this->listingDAO->save($listing);

        return $listing;
    }

    /**
     * When a listing is bought we mark all the listed ticket as bought by the actor user.
     *
     * @param Listing $listing
     * @param User $user
     * @return Listing
     */
    public function buyListing(Listing $listing, User $user)
    {
        $now = new \DateTime();

        $this->checkForAlreadyBoughtListing($listing);

        $this->markAllTicketsOnListingAsBought($listing, $user, $now);

        // Persist the listing (cascading to the tickets)
        $this->listingDAO->save($listing);

        return $listing;
    }

    /**
     * Throws an exception if the list of barcodes contains duplicates
     *
     * @param $barcodes
     */
    private function checkForDuplicatedBarcodes($barcodes)
    {
        if (count($barcodes) !== count(array_unique($barcodes))) {
            throw new HttpException(400, "A listing cannot contain duplicated barcodes");
        }
    }

    /**
     * Throws an exception if the list of barcodes contains barcodes currently listed in active listings
     *
     * @param $barcodes
     */
    private function checkForBarcodesInActiveListings($barcodes)
    {
        if ($this->barcodeDAO->checkIfBarcodesAreOnActiveListings($barcodes)) {
            throw new HttpException(400, "A listing cannot contain barcodes already associated to tickets on active listings");
        }
    }

    /**
     * Throws an exception if the user can not list one or more of the provided barcodes.
     *
     * @param User $user
     * @param $barcodes
     */
    private function checkIfUserCanBuyedPreviouslyListedBarcodes(User $user, $barcodes)
    {
        /**
         * The listing can be created only if the current user is the last buyer of ALL the tickets
         * (associated to the barcodes provided) WHICH HAVE BEEN SOLD here previously.
         */
        $lastBuyersIds = $this->barcodeDAO->getUserIdOfBarcodeAssociatedLastBuyer($barcodes);

        foreach ($lastBuyersIds as $buyerId) {
            if ($user->getId() != $buyerId) {
                // A ticket has been bought and the current user is not the last buyer
                throw new HttpException(400, "You cannot add a listing with a ticket purchased @ ts by other user");
            }
        }
    }

    /**
     * Returns a Listing entity with all the required associations (tickets & barcodes)
     *
     * @param User $user
     * @param $sellingPrice
     * @param $description
     * @param $tickets
     * @return Listing
     */
    private function createNewListing(User $user, $sellingPrice, $description, $tickets)
    {
        $listing = new Listing();

        $listing
            ->setDescription($description)
            ->setSellingPrice($sellingPrice)
            ->setUser($user);

        // Process the tickets
        foreach ($tickets as $ticketAsArray) {
            $ticket = new Ticket();

            $ticket
                ->setCreatedAt(new \DateTime())
                ->setListing($listing);

            // Process the barcodes (if present)
            if (array_key_exists('barcodes', $ticketAsArray)) {
                $barcodes = $ticketAsArray['barcodes'];

                foreach ($barcodes as $code) {
                    // Create the barcode and link it to the ticket
                    $barcode = new Barcode();
                    $barcode
                        ->setBarcode($code)
                        ->setTicket($ticket);

                    // Add the barcode to the ticket
                    $ticket->addBarcode($barcode);
                }
            }

            // Add the ticket to the listing
            $listing->addTicket($ticket);
        }
        return $listing;
    }

    /**
     * @param Listing $listing
     * @param User $user
     * @param $now
     */
    private function markAllTicketsOnListingAsBought(Listing $listing, User $user, $now)
    {
        $tickets = $listing->getTickets();
        /** @var Ticket $ticket */
        foreach ($tickets as $ticket) {
            // Mark all tickets as bought
            $ticket
                ->setBoughtAt($now)
                ->setBoughtByUser($user);
        }
    }

    /**
     * Throws an exception if the provided Listed entity has already been bought
     *
     * @param Listing $listing
     */
    private function checkForAlreadyBoughtListing(Listing $listing)
    {
        // Avoid buying an already purchased listing
        if ($listing->isBought()) {
            throw new HttpException(400, "You cannot buy and already bought listing");
        }
    }
}