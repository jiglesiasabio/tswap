<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_69ec298dbde29cd028cd6584b595b8aa673edbb0286af5361736af5333bf73a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7b43c4a21b9369ed970bbf58237a0ab7241871637dd090e3a3a2eab30e45d87 = $this->env->getExtension("native_profiler");
        $__internal_c7b43c4a21b9369ed970bbf58237a0ab7241871637dd090e3a3a2eab30e45d87->enter($__internal_c7b43c4a21b9369ed970bbf58237a0ab7241871637dd090e3a3a2eab30e45d87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_c7b43c4a21b9369ed970bbf58237a0ab7241871637dd090e3a3a2eab30e45d87->leave($__internal_c7b43c4a21b9369ed970bbf58237a0ab7241871637dd090e3a3a2eab30e45d87_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
