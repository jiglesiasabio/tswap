<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_42b7af7a29e91d1a8e9820f9ee292f00ac3325c292ecc39487c14b04ac852bdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7831fa1b02033e106fea03e0b744ded8ba5e32d97a849208f4cd768824f7d77c = $this->env->getExtension("native_profiler");
        $__internal_7831fa1b02033e106fea03e0b744ded8ba5e32d97a849208f4cd768824f7d77c->enter($__internal_7831fa1b02033e106fea03e0b744ded8ba5e32d97a849208f4cd768824f7d77c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_7831fa1b02033e106fea03e0b744ded8ba5e32d97a849208f4cd768824f7d77c->leave($__internal_7831fa1b02033e106fea03e0b744ded8ba5e32d97a849208f4cd768824f7d77c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
