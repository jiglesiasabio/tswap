<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_7c262debb27a6f5d87bfd5730446702ecdc5fff9948bc62d00d44a9c9f1cb83e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa89a7255a1d8c9f5e92b0fbdcf2f5500a103f79ad413f928dfc78478a305e4b = $this->env->getExtension("native_profiler");
        $__internal_fa89a7255a1d8c9f5e92b0fbdcf2f5500a103f79ad413f928dfc78478a305e4b->enter($__internal_fa89a7255a1d8c9f5e92b0fbdcf2f5500a103f79ad413f928dfc78478a305e4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_fa89a7255a1d8c9f5e92b0fbdcf2f5500a103f79ad413f928dfc78478a305e4b->leave($__internal_fa89a7255a1d8c9f5e92b0fbdcf2f5500a103f79ad413f928dfc78478a305e4b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
