<?php

/* WebAppBundle:Default:purchase.html.twig */
class __TwigTemplate_7592f87189f43d6685c2bb60ce584897b83f1820f9aac82b7c03152c2a57009a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:formPage.html.twig", "WebAppBundle:Default:purchase.html.twig", 1);
        $this->blocks = array(
            'description' => array($this, 'block_description'),
            'form' => array($this, 'block_form'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:formPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c502f0584413074f24aa2ea604897e8a9aed9fe77adc591f0adb354217d63ac = $this->env->getExtension("native_profiler");
        $__internal_3c502f0584413074f24aa2ea604897e8a9aed9fe77adc591f0adb354217d63ac->enter($__internal_3c502f0584413074f24aa2ea604897e8a9aed9fe77adc591f0adb354217d63ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:purchase.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c502f0584413074f24aa2ea604897e8a9aed9fe77adc591f0adb354217d63ac->leave($__internal_3c502f0584413074f24aa2ea604897e8a9aed9fe77adc591f0adb354217d63ac_prof);

    }

    // line 3
    public function block_description($context, array $blocks = array())
    {
        $__internal_a603eee95c81b409a7e114d09ede63f7cd310e232fb81361a7ddfa0163597f50 = $this->env->getExtension("native_profiler");
        $__internal_a603eee95c81b409a7e114d09ede63f7cd310e232fb81361a7ddfa0163597f50->enter($__internal_a603eee95c81b409a7e114d09ede63f7cd310e232fb81361a7ddfa0163597f50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 4
        echo "    <h3>Purchase</h3>
    <p>To purchase a Listing specify the following data.</p>
";
        
        $__internal_a603eee95c81b409a7e114d09ede63f7cd310e232fb81361a7ddfa0163597f50->leave($__internal_a603eee95c81b409a7e114d09ede63f7cd310e232fb81361a7ddfa0163597f50_prof);

    }

    // line 8
    public function block_form($context, array $blocks = array())
    {
        $__internal_6930142679965bad7c4b44bdaee740e489b7d9e7f71f94eb0405581c9b24f93d = $this->env->getExtension("native_profiler");
        $__internal_6930142679965bad7c4b44bdaee740e489b7d9e7f71f94eb0405581c9b24f93d->enter($__internal_6930142679965bad7c4b44bdaee740e489b7d9e7f71f94eb0405581c9b24f93d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 9
        echo "    <form id=\"user-form\">
        user id:<br>
        <input type=\"text\" id=\"userId\"><br>

        Listing id:<br>
        <input type=\"text\" id=\"listingId\"><br>
    </form>
";
        
        $__internal_6930142679965bad7c4b44bdaee740e489b7d9e7f71f94eb0405581c9b24f93d->leave($__internal_6930142679965bad7c4b44bdaee740e489b7d9e7f71f94eb0405581c9b24f93d_prof);

    }

    // line 18
    public function block_js($context, array $blocks = array())
    {
        $__internal_4b1680dcb4fb38102e57c4a4648b73913ace0c6c714849b42739696674c87318 = $this->env->getExtension("native_profiler");
        $__internal_4b1680dcb4fb38102e57c4a4648b73913ace0c6c714849b42739696674c87318->enter($__internal_4b1680dcb4fb38102e57c4a4648b73913ace0c6c714849b42739696674c87318_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        // line 19
        echo "    <script>
        function printResponse(data) {
            \$('#console').html(JSON.stringify(data, null, 2));
        }
        \$(document).ready(function () {
            \$('#post').click(function () {

                var userId = \$('#userId').val();
                var listingId = \$('#listingId').val();

                if (userId && listingId) {
                    \$.ajax({
                        type: \"POST\",
                        url: \"/listing/\" + listingId + \"/buy\",
                        data: {\"user_id\": userId},
                        success: function (data) {
                            printResponse(data)
                        },
                        error: function (data) {
                            printResponse(data.responseJSON)
                        },
                        dataType: \"json\"
                    });
                }
            });
        });
    </script>
";
        
        $__internal_4b1680dcb4fb38102e57c4a4648b73913ace0c6c714849b42739696674c87318->leave($__internal_4b1680dcb4fb38102e57c4a4648b73913ace0c6c714849b42739696674c87318_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:purchase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 19,  71 => 18,  57 => 9,  51 => 8,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:formPage.html.twig' %}*/
/* */
/* {% block description %}*/
/*     <h3>Purchase</h3>*/
/*     <p>To purchase a Listing specify the following data.</p>*/
/* {% endblock %}*/
/* */
/* {% block form %}*/
/*     <form id="user-form">*/
/*         user id:<br>*/
/*         <input type="text" id="userId"><br>*/
/* */
/*         Listing id:<br>*/
/*         <input type="text" id="listingId"><br>*/
/*     </form>*/
/* {% endblock %}*/
/* */
/* {% block js %}*/
/*     <script>*/
/*         function printResponse(data) {*/
/*             $('#console').html(JSON.stringify(data, null, 2));*/
/*         }*/
/*         $(document).ready(function () {*/
/*             $('#post').click(function () {*/
/* */
/*                 var userId = $('#userId').val();*/
/*                 var listingId = $('#listingId').val();*/
/* */
/*                 if (userId && listingId) {*/
/*                     $.ajax({*/
/*                         type: "POST",*/
/*                         url: "/listing/" + listingId + "/buy",*/
/*                         data: {"user_id": userId},*/
/*                         success: function (data) {*/
/*                             printResponse(data)*/
/*                         },*/
/*                         error: function (data) {*/
/*                             printResponse(data.responseJSON)*/
/*                         },*/
/*                         dataType: "json"*/
/*                     });*/
/*                 }*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
