<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 09:21
 */

namespace TicketListingBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends Controller
{

    protected function errorResponse($error)
    {
        $responseBody = [
            "status" => "error",
            "message" => $error
        ];
        return new JsonResponse($responseBody, 400);
    }

    protected function successResponse($data)
    {
        $responseBody = [
            "status" => "success",
            "data" => $data
        ];
        return new JsonResponse($responseBody, 200);
    }
}