<?php

/* WebAppBundle:Default:usersList.html.twig */
class __TwigTemplate_7a889d678977a772698fa31e345ca8609a5eb8187cc9b97d6fb66ac8271890c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:listingPage.html.twig", "WebAppBundle:Default:usersList.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'description' => array($this, 'block_description'),
            'list' => array($this, 'block_list'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:listingPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d839344246af46b68e508b22e68fe99304d0cf841ddc742747277628646c5fb = $this->env->getExtension("native_profiler");
        $__internal_5d839344246af46b68e508b22e68fe99304d0cf841ddc742747277628646c5fb->enter($__internal_5d839344246af46b68e508b22e68fe99304d0cf841ddc742747277628646c5fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:usersList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5d839344246af46b68e508b22e68fe99304d0cf841ddc742747277628646c5fb->leave($__internal_5d839344246af46b68e508b22e68fe99304d0cf841ddc742747277628646c5fb_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_d7fde19996d192c3aaf9e079fbe43b4a098d2f7cfd7e619eb866aa2d558bb35b = $this->env->getExtension("native_profiler");
        $__internal_d7fde19996d192c3aaf9e079fbe43b4a098d2f7cfd7e619eb866aa2d558bb35b->enter($__internal_d7fde19996d192c3aaf9e079fbe43b4a098d2f7cfd7e619eb866aa2d558bb35b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"row marketing\">
    <div class=\"col-lg-12\">

        ";
        // line 7
        $this->displayBlock('description', $context, $blocks);
        // line 11
        echo "
        <hr>

        ";
        // line 14
        $this->displayBlock('list', $context, $blocks);
        // line 24
        echo "    </div>

";
        
        $__internal_d7fde19996d192c3aaf9e079fbe43b4a098d2f7cfd7e619eb866aa2d558bb35b->leave($__internal_d7fde19996d192c3aaf9e079fbe43b4a098d2f7cfd7e619eb866aa2d558bb35b_prof);

    }

    // line 7
    public function block_description($context, array $blocks = array())
    {
        $__internal_5697b843216403defc6e672b9ca1e9a9777213c8de9f0e1389b7d602a2efa919 = $this->env->getExtension("native_profiler");
        $__internal_5697b843216403defc6e672b9ca1e9a9777213c8de9f0e1389b7d602a2efa919->enter($__internal_5697b843216403defc6e672b9ca1e9a9777213c8de9f0e1389b7d602a2efa919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 8
        echo "            <h3>Users list</h3>
            <p>Here you can see a list of all the users.</p>
        ";
        
        $__internal_5697b843216403defc6e672b9ca1e9a9777213c8de9f0e1389b7d602a2efa919->leave($__internal_5697b843216403defc6e672b9ca1e9a9777213c8de9f0e1389b7d602a2efa919_prof);

    }

    // line 14
    public function block_list($context, array $blocks = array())
    {
        $__internal_e304ada80454523f66028a5c773b1556c56329be88f91262357509d3931cd25b = $this->env->getExtension("native_profiler");
        $__internal_e304ada80454523f66028a5c773b1556c56329be88f91262357509d3931cd25b->enter($__internal_e304ada80454523f66028a5c773b1556c56329be88f91262357509d3931cd25b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        // line 15
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 16
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-body\">
                            <h4>#";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</h4>
                            Name: ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "name", array()), "html", null, true);
            echo "<br>
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        ";
        
        $__internal_e304ada80454523f66028a5c773b1556c56329be88f91262357509d3931cd25b->leave($__internal_e304ada80454523f66028a5c773b1556c56329be88f91262357509d3931cd25b_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:usersList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 23,  99 => 19,  95 => 18,  91 => 16,  86 => 15,  80 => 14,  71 => 8,  65 => 7,  56 => 24,  54 => 14,  49 => 11,  47 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:listingPage.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <div class="row marketing">*/
/*     <div class="col-lg-12">*/
/* */
/*         {% block description %}*/
/*             <h3>Users list</h3>*/
/*             <p>Here you can see a list of all the users.</p>*/
/*         {% endblock %}*/
/* */
/*         <hr>*/
/* */
/*         {% block list %}*/
/*                 {% for user in users %}*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-body">*/
/*                             <h4>#{{ user.id }}</h4>*/
/*                             Name: {{ user.name }}<br>*/
/*                         </div>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*         {% endblock %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
