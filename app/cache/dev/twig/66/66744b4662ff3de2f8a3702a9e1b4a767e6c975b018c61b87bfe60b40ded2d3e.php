<?php

/* WebAppBundle:Default:listingsList.html.twig */
class __TwigTemplate_fd72b53d4ddb7f06622eba66591eb86bb210d8909b9e738863d858556a5b3f8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:listingPage.html.twig", "WebAppBundle:Default:listingsList.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'description' => array($this, 'block_description'),
            'list' => array($this, 'block_list'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:listingPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8979e422af7d7f510bf9f61e5206d62f911458d653adc78e6292ea7137e5c9d = $this->env->getExtension("native_profiler");
        $__internal_d8979e422af7d7f510bf9f61e5206d62f911458d653adc78e6292ea7137e5c9d->enter($__internal_d8979e422af7d7f510bf9f61e5206d62f911458d653adc78e6292ea7137e5c9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:listingsList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8979e422af7d7f510bf9f61e5206d62f911458d653adc78e6292ea7137e5c9d->leave($__internal_d8979e422af7d7f510bf9f61e5206d62f911458d653adc78e6292ea7137e5c9d_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_21d7fb5df449d24a137d08f61e5cd4f6e5fff5de0b284598a9785fa820550748 = $this->env->getExtension("native_profiler");
        $__internal_21d7fb5df449d24a137d08f61e5cd4f6e5fff5de0b284598a9785fa820550748->enter($__internal_21d7fb5df449d24a137d08f61e5cd4f6e5fff5de0b284598a9785fa820550748_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"row marketing\">
    <div class=\"col-lg-12\">

        ";
        // line 7
        $this->displayBlock('description', $context, $blocks);
        // line 11
        echo "
        <hr>

        ";
        // line 14
        $this->displayBlock('list', $context, $blocks);
        // line 52
        echo "    </div>

";
        
        $__internal_21d7fb5df449d24a137d08f61e5cd4f6e5fff5de0b284598a9785fa820550748->leave($__internal_21d7fb5df449d24a137d08f61e5cd4f6e5fff5de0b284598a9785fa820550748_prof);

    }

    // line 7
    public function block_description($context, array $blocks = array())
    {
        $__internal_4d63843e39b6b5ff119829f183f8e4bd0711e1173dfbb90907bf2c6a6ada3168 = $this->env->getExtension("native_profiler");
        $__internal_4d63843e39b6b5ff119829f183f8e4bd0711e1173dfbb90907bf2c6a6ada3168->enter($__internal_4d63843e39b6b5ff119829f183f8e4bd0711e1173dfbb90907bf2c6a6ada3168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 8
        echo "            <h3>Listings list</h3>
            <p>Here you can see a list of all the current listings.</p>
        ";
        
        $__internal_4d63843e39b6b5ff119829f183f8e4bd0711e1173dfbb90907bf2c6a6ada3168->leave($__internal_4d63843e39b6b5ff119829f183f8e4bd0711e1173dfbb90907bf2c6a6ada3168_prof);

    }

    // line 14
    public function block_list($context, array $blocks = array())
    {
        $__internal_b412c9832010c277d53b2314474b451b19467ee2b9129f615655b71a353c30ce = $this->env->getExtension("native_profiler");
        $__internal_b412c9832010c277d53b2314474b451b19467ee2b9129f615655b71a353c30ce->enter($__internal_b412c9832010c277d53b2314474b451b19467ee2b9129f615655b71a353c30ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        // line 15
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listings"]) ? $context["listings"] : $this->getContext($context, "listings")));
        foreach ($context['_seq'] as $context["_key"] => $context["listing"]) {
            // line 16
            echo "                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <h4>#";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["listing"], "id", array()), "html", null, true);
            echo "</h4>
                        Description: ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["listing"], "description", array()), "html", null, true);
            echo "<br>
                        Price: ";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["listing"], "sellingPrice", array()), "html", null, true);
            echo " Coins<br>

                        Created by: user #";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["listing"], "user", array()), "id", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["listing"], "user", array()), "name", array()), "html", null, true);
            echo ")

                        <hr>

                            ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["listing"], "tickets", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["ticket"]) {
                // line 27
                echo "                                Ticket #";
                echo twig_escape_filter($this->env, $this->getAttribute($context["ticket"], "id", array()), "html", null, true);
                echo " -

                                ";
                // line 29
                if ($this->getAttribute($context["ticket"], "boughtAt", array())) {
                    // line 30
                    echo "                                    Bought at: ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["ticket"], "boughtAt", array()), "m/d/Y"), "html", null, true);
                    echo "

                                    ";
                    // line 32
                    if ($this->getAttribute($context["ticket"], "boughtByUser", array())) {
                        // line 33
                        echo "                                        by user #";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["ticket"], "boughtByUser", array()), "id", array()), "html", null, true);
                        echo " (";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["ticket"], "boughtByUser", array()), "name", array()), "html", null, true);
                        echo ")
                                    ";
                    }
                    // line 35
                    echo "                                ";
                }
                // line 36
                echo "

                                <br>


                                <ul>
                                    ";
                // line 42
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["ticket"], "barcodes", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["barcode"]) {
                    // line 43
                    echo "                                        <li>bc: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["barcode"], "barcode", array()), "html", null, true);
                    echo "</li>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['barcode'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "                                </ul>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ticket'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listing'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "        ";
        
        $__internal_b412c9832010c277d53b2314474b451b19467ee2b9129f615655b71a353c30ce->leave($__internal_b412c9832010c277d53b2314474b451b19467ee2b9129f615655b71a353c30ce_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:listingsList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 51,  176 => 47,  169 => 45,  160 => 43,  156 => 42,  148 => 36,  145 => 35,  137 => 33,  135 => 32,  129 => 30,  127 => 29,  121 => 27,  117 => 26,  108 => 22,  103 => 20,  99 => 19,  95 => 18,  91 => 16,  86 => 15,  80 => 14,  71 => 8,  65 => 7,  56 => 52,  54 => 14,  49 => 11,  47 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:listingPage.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <div class="row marketing">*/
/*     <div class="col-lg-12">*/
/* */
/*         {% block description %}*/
/*             <h3>Listings list</h3>*/
/*             <p>Here you can see a list of all the current listings.</p>*/
/*         {% endblock %}*/
/* */
/*         <hr>*/
/* */
/*         {% block list %}*/
/*             {% for listing in listings %}*/
/*                 <div class="panel panel-default">*/
/*                     <div class="panel-body">*/
/*                         <h4>#{{ listing.id }}</h4>*/
/*                         Description: {{ listing.description }}<br>*/
/*                         Price: {{ listing.sellingPrice }} Coins<br>*/
/* */
/*                         Created by: user #{{ listing.user.id }} ({{ listing.user.name }})*/
/* */
/*                         <hr>*/
/* */
/*                             {% for ticket in listing.tickets %}*/
/*                                 Ticket #{{ ticket.id }} -*/
/* */
/*                                 {% if ticket.boughtAt %}*/
/*                                     Bought at: {{ ticket.boughtAt|date("m/d/Y") }}*/
/* */
/*                                     {% if ticket.boughtByUser %}*/
/*                                         by user #{{ ticket.boughtByUser.id }} ({{ ticket.boughtByUser.name }})*/
/*                                     {% endif %}*/
/*                                 {% endif %}*/
/* */
/* */
/*                                 <br>*/
/* */
/* */
/*                                 <ul>*/
/*                                     {% for barcode in ticket.barcodes %}*/
/*                                         <li>bc: {{ barcode.barcode }}</li>*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             {% endfor %}*/
/* */
/*                     </div>*/
/*                 </div>*/
/*             {% endfor %}*/
/*         {% endblock %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
