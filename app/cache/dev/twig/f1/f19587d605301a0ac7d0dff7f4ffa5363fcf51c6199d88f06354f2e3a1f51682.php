<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_6560823f980455e60b44548bfea610fc4559c8300a1e0e5c3be4bb61d2ab56a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1ae99eaf881de6d5a7234e2167040077160086d17fe1462ca2c874b8cc2e427 = $this->env->getExtension("native_profiler");
        $__internal_b1ae99eaf881de6d5a7234e2167040077160086d17fe1462ca2c874b8cc2e427->enter($__internal_b1ae99eaf881de6d5a7234e2167040077160086d17fe1462ca2c874b8cc2e427_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_b1ae99eaf881de6d5a7234e2167040077160086d17fe1462ca2c874b8cc2e427->leave($__internal_b1ae99eaf881de6d5a7234e2167040077160086d17fe1462ca2c874b8cc2e427_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
