<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_2e24bf097717940dd5bd38a2a263014f9133360b9739200e1458cd03b3cfa096 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ded8f53c4d503ea74e5e282257098b868e7751aa306d5ccea770fc9207313900 = $this->env->getExtension("native_profiler");
        $__internal_ded8f53c4d503ea74e5e282257098b868e7751aa306d5ccea770fc9207313900->enter($__internal_ded8f53c4d503ea74e5e282257098b868e7751aa306d5ccea770fc9207313900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_ded8f53c4d503ea74e5e282257098b868e7751aa306d5ccea770fc9207313900->leave($__internal_ded8f53c4d503ea74e5e282257098b868e7751aa306d5ccea770fc9207313900_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
