<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_c47d75ecbcf37001c95ef2335504bbab90b6a263fde2218a62c5b04f6406dcf4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb26765cdba33d73f14e9db6b508c15bb6d58dfe04de94799a62e8505f67889b = $this->env->getExtension("native_profiler");
        $__internal_cb26765cdba33d73f14e9db6b508c15bb6d58dfe04de94799a62e8505f67889b->enter($__internal_cb26765cdba33d73f14e9db6b508c15bb6d58dfe04de94799a62e8505f67889b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_cb26765cdba33d73f14e9db6b508c15bb6d58dfe04de94799a62e8505f67889b->leave($__internal_cb26765cdba33d73f14e9db6b508c15bb6d58dfe04de94799a62e8505f67889b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
