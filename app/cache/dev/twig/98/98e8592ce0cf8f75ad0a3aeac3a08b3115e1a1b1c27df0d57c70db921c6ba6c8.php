<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_25a03a2eb366f12c2237491b56029a3deb515e6dc60556f50b6811d67c2ef5c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c78141b6ef64a5edd0c38d2aee7b89aa772f6cda318c29985cd11a00119cc0d = $this->env->getExtension("native_profiler");
        $__internal_4c78141b6ef64a5edd0c38d2aee7b89aa772f6cda318c29985cd11a00119cc0d->enter($__internal_4c78141b6ef64a5edd0c38d2aee7b89aa772f6cda318c29985cd11a00119cc0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_4c78141b6ef64a5edd0c38d2aee7b89aa772f6cda318c29985cd11a00119cc0d->leave($__internal_4c78141b6ef64a5edd0c38d2aee7b89aa772f6cda318c29985cd11a00119cc0d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
