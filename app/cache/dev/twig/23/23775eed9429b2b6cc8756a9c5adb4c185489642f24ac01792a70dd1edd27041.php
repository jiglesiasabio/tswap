<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_4961a3e35aed987227ffa399522a2e806239c9ac6fb3f6263181d39145df8b66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83b6982e99c229664b611b4e3b5f5a1caefdf13bd480bf0fa61c5bb3d44d1bab = $this->env->getExtension("native_profiler");
        $__internal_83b6982e99c229664b611b4e3b5f5a1caefdf13bd480bf0fa61c5bb3d44d1bab->enter($__internal_83b6982e99c229664b611b4e3b5f5a1caefdf13bd480bf0fa61c5bb3d44d1bab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_83b6982e99c229664b611b4e3b5f5a1caefdf13bd480bf0fa61c5bb3d44d1bab->leave($__internal_83b6982e99c229664b611b4e3b5f5a1caefdf13bd480bf0fa61c5bb3d44d1bab_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
