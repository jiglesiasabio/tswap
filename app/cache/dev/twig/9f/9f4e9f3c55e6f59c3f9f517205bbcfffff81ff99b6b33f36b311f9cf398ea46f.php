<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_aa9fbf41e7a810bb549d238a96177c14037de11f4c777515794d1bd45d215029 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_29629565571bdd25fce6ec77f7db8cbcd0907c9a5dd192a5919e87d15c2e0a00 = $this->env->getExtension("native_profiler");
        $__internal_29629565571bdd25fce6ec77f7db8cbcd0907c9a5dd192a5919e87d15c2e0a00->enter($__internal_29629565571bdd25fce6ec77f7db8cbcd0907c9a5dd192a5919e87d15c2e0a00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_29629565571bdd25fce6ec77f7db8cbcd0907c9a5dd192a5919e87d15c2e0a00->leave($__internal_29629565571bdd25fce6ec77f7db8cbcd0907c9a5dd192a5919e87d15c2e0a00_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
