<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_a47c0d7a356baee09399b4d5630240ecdbf72dad7b15665ea26adfdd6d75c885 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7fe8aa5ba95d02751592013c6cb4931f94a195d4cec3ab669b0c596bec93872 = $this->env->getExtension("native_profiler");
        $__internal_d7fe8aa5ba95d02751592013c6cb4931f94a195d4cec3ab669b0c596bec93872->enter($__internal_d7fe8aa5ba95d02751592013c6cb4931f94a195d4cec3ab669b0c596bec93872_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d7fe8aa5ba95d02751592013c6cb4931f94a195d4cec3ab669b0c596bec93872->leave($__internal_d7fe8aa5ba95d02751592013c6cb4931f94a195d4cec3ab669b0c596bec93872_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
