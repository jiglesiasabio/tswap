<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_c55dc83ad23308fbaaf47d72fd8d4ac846ac3a808b3a60e305688ef94f4df733 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a021417c1f13d844ef3915a9157da55bd078ca40c52aeb6c94de5b6a784226e = $this->env->getExtension("native_profiler");
        $__internal_5a021417c1f13d844ef3915a9157da55bd078ca40c52aeb6c94de5b6a784226e->enter($__internal_5a021417c1f13d844ef3915a9157da55bd078ca40c52aeb6c94de5b6a784226e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_5a021417c1f13d844ef3915a9157da55bd078ca40c52aeb6c94de5b6a784226e->leave($__internal_5a021417c1f13d844ef3915a9157da55bd078ca40c52aeb6c94de5b6a784226e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
