<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_b2bf73f78d41092e1362514704c5fdacc5eab396078a920fe1b6b153d3084fbd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e0f3c1635b66fef1c347c2759f5adf915759d633ae7de34a624ce22939de606 = $this->env->getExtension("native_profiler");
        $__internal_7e0f3c1635b66fef1c347c2759f5adf915759d633ae7de34a624ce22939de606->enter($__internal_7e0f3c1635b66fef1c347c2759f5adf915759d633ae7de34a624ce22939de606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_7e0f3c1635b66fef1c347c2759f5adf915759d633ae7de34a624ce22939de606->leave($__internal_7e0f3c1635b66fef1c347c2759f5adf915759d633ae7de34a624ce22939de606_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
