<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_468cb6daab3de093e19f986acaf5ba66fc72b0a643da93fdf8db10f3db598d23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dbdae58ee5f55e68110fbb2dcda51a90654696faa56f60e899408757af1f18b0 = $this->env->getExtension("native_profiler");
        $__internal_dbdae58ee5f55e68110fbb2dcda51a90654696faa56f60e899408757af1f18b0->enter($__internal_dbdae58ee5f55e68110fbb2dcda51a90654696faa56f60e899408757af1f18b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_dbdae58ee5f55e68110fbb2dcda51a90654696faa56f60e899408757af1f18b0->leave($__internal_dbdae58ee5f55e68110fbb2dcda51a90654696faa56f60e899408757af1f18b0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
