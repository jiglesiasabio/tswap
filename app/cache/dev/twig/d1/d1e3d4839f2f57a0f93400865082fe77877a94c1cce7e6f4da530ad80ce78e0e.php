<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_e1f4c50cfeb4576cbce77a9c1509b35c1e20b6e4bd4702fdea3ad5868c9f1ed5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_345b098ca4e5ccf5e917e27ec1ff5458aebd396001c0a7f49ab83ec03f01a901 = $this->env->getExtension("native_profiler");
        $__internal_345b098ca4e5ccf5e917e27ec1ff5458aebd396001c0a7f49ab83ec03f01a901->enter($__internal_345b098ca4e5ccf5e917e27ec1ff5458aebd396001c0a7f49ab83ec03f01a901_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_345b098ca4e5ccf5e917e27ec1ff5458aebd396001c0a7f49ab83ec03f01a901->leave($__internal_345b098ca4e5ccf5e917e27ec1ff5458aebd396001c0a7f49ab83ec03f01a901_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
