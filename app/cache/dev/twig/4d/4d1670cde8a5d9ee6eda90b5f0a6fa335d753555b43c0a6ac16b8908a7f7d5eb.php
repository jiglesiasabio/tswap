<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_cbb420a09036c365938e896107bb1ce9c4c979452e1aa1dc4773296249b473ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c6b039cb2fe953168d4efcc6dcc58e8299290b6b2eb64b800bcd1cf35615491 = $this->env->getExtension("native_profiler");
        $__internal_9c6b039cb2fe953168d4efcc6dcc58e8299290b6b2eb64b800bcd1cf35615491->enter($__internal_9c6b039cb2fe953168d4efcc6dcc58e8299290b6b2eb64b800bcd1cf35615491_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_9c6b039cb2fe953168d4efcc6dcc58e8299290b6b2eb64b800bcd1cf35615491->leave($__internal_9c6b039cb2fe953168d4efcc6dcc58e8299290b6b2eb64b800bcd1cf35615491_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
