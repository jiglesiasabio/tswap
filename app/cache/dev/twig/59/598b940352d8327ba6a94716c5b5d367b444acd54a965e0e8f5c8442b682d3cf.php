<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_4a8b55450f0b81f339630c82fe02f7a54c0408ce86768aebc1044f646d30662d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e08ed75e09ec1291810b950643a1250518dbb2aebebccdc5ee4b53ab78304313 = $this->env->getExtension("native_profiler");
        $__internal_e08ed75e09ec1291810b950643a1250518dbb2aebebccdc5ee4b53ab78304313->enter($__internal_e08ed75e09ec1291810b950643a1250518dbb2aebebccdc5ee4b53ab78304313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_e08ed75e09ec1291810b950643a1250518dbb2aebebccdc5ee4b53ab78304313->leave($__internal_e08ed75e09ec1291810b950643a1250518dbb2aebebccdc5ee4b53ab78304313_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
