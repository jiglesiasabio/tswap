<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/app')) {
            // web_app_index
            if (rtrim($pathinfo, '/') === '/app') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'web_app_index');
                }

                return array (  '_controller' => 'WebAppBundle\\Controller\\WebAppController::indexAction',  '_route' => 'web_app_index',);
            }

            // web_app_users
            if ($pathinfo === '/app/users') {
                return array (  '_controller' => 'WebAppBundle\\Controller\\WebAppController::usersAction',  '_route' => 'web_app_users',);
            }

            // web_app_listings
            if ($pathinfo === '/app/listings') {
                return array (  '_controller' => 'WebAppBundle\\Controller\\WebAppController::listingsAction',  '_route' => 'web_app_listings',);
            }

            // web_app_purchase
            if ($pathinfo === '/app/purchase') {
                return array (  '_controller' => 'WebAppBundle\\Controller\\WebAppController::purchaseAction',  '_route' => 'web_app_purchase',);
            }

            // web_app_users_list
            if ($pathinfo === '/app/users_list') {
                return array (  '_controller' => 'WebAppBundle\\Controller\\WebAppController::usersListAction',  '_route' => 'web_app_users_list',);
            }

            // web_app_listings_list
            if ($pathinfo === '/app/listings_list') {
                return array (  '_controller' => 'WebAppBundle\\Controller\\WebAppController::listingsListAction',  '_route' => 'web_app_listings_list',);
            }

        }

        if (0 === strpos($pathinfo, '/barcode')) {
            // barcode_post
            if ($pathinfo === '/barcode') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_barcode_post;
                }

                return array (  '_controller' => 'TicketListingBundle\\Controller\\BarcodeController::createBarcodeAction',  '_format' => 'json',  '_route' => 'barcode_post',);
            }
            not_barcode_post:

            // barcode_post_slashed
            if ($pathinfo === '/barcode/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_barcode_post_slashed;
                }

                return array (  '_controller' => 'TicketListingBundle\\Controller\\BarcodeController::createBarcodeAction',  '_format' => 'json',  '_route' => 'barcode_post_slashed',);
            }
            not_barcode_post_slashed:

        }

        // ticketlisting_default_index
        if ($pathinfo === '/app.html') {
            return array (  '_controller' => 'TicketListingBundle\\Controller\\DefaultController::indexAction',  '_format' => 'json',  '_route' => 'ticketlisting_default_index',);
        }

        if (0 === strpos($pathinfo, '/listing')) {
            // listing_post
            if ($pathinfo === '/listing') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_listing_post;
                }

                return array (  '_controller' => 'TicketListingBundle\\Controller\\ListingController::createListingAction',  '_format' => 'json',  '_route' => 'listing_post',);
            }
            not_listing_post:

            // listing_post_slashed
            if ($pathinfo === '/listing/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_listing_post_slashed;
                }

                return array (  '_controller' => 'TicketListingBundle\\Controller\\ListingController::createListingAction',  '_format' => 'json',  '_route' => 'listing_post_slashed',);
            }
            not_listing_post_slashed:

            // buy_listing_post
            if (preg_match('#^/listing/(?P<id>[^/]++)/buy$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_buy_listing_post;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'buy_listing_post')), array (  '_controller' => 'TicketListingBundle\\Controller\\ListingController::buyListingAction',  '_format' => 'json',));
            }
            not_buy_listing_post:

            // buy_listing_post_slashed
            if (preg_match('#^/listing/(?P<id>[^/]++)/buy/$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_buy_listing_post_slashed;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'buy_listing_post_slashed')), array (  '_controller' => 'TicketListingBundle\\Controller\\ListingController::buyListingAction',  '_format' => 'json',));
            }
            not_buy_listing_post_slashed:

        }

        if (0 === strpos($pathinfo, '/user')) {
            // user_post
            if ($pathinfo === '/user') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_post;
                }

                return array (  '_controller' => 'TicketListingBundle\\Controller\\UserController::createUserAction',  '_format' => 'json',  '_route' => 'user_post',);
            }
            not_user_post:

            // user_post_slashed
            if ($pathinfo === '/user/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_post_slashed;
                }

                return array (  '_controller' => 'TicketListingBundle\\Controller\\UserController::createUserAction',  '_format' => 'json',  '_route' => 'user_post_slashed',);
            }
            not_user_post_slashed:

            // user_get
            if (preg_match('#^/user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_user_get;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_get')), array (  '_controller' => 'TicketListingBundle\\Controller\\UserController::getUserAction',  '_format' => 'json',));
            }
            not_user_get:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
