<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_54ad276a9c83cd96cbfb99d0cf93c2c59e708d79c34e6ef00c4c0bcb1c3ffee3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65512dfc16d8d6869efd5497f8d9c7e4b23e97dde13e56fa7132b3b0fd6b5e49 = $this->env->getExtension("native_profiler");
        $__internal_65512dfc16d8d6869efd5497f8d9c7e4b23e97dde13e56fa7132b3b0fd6b5e49->enter($__internal_65512dfc16d8d6869efd5497f8d9c7e4b23e97dde13e56fa7132b3b0fd6b5e49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_65512dfc16d8d6869efd5497f8d9c7e4b23e97dde13e56fa7132b3b0fd6b5e49->leave($__internal_65512dfc16d8d6869efd5497f8d9c7e4b23e97dde13e56fa7132b3b0fd6b5e49_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
