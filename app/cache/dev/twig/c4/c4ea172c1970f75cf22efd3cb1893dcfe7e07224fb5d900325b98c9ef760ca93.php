<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_08c64c92f446c805014dde288c3ec8aa1aaf14bea6d82cc584ce90d55ced46dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_00180481f4f077e3331b97cd0807b9aa0603ffee661d23c825fa7e52289fda07 = $this->env->getExtension("native_profiler");
        $__internal_00180481f4f077e3331b97cd0807b9aa0603ffee661d23c825fa7e52289fda07->enter($__internal_00180481f4f077e3331b97cd0807b9aa0603ffee661d23c825fa7e52289fda07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_00180481f4f077e3331b97cd0807b9aa0603ffee661d23c825fa7e52289fda07->leave($__internal_00180481f4f077e3331b97cd0807b9aa0603ffee661d23c825fa7e52289fda07_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
