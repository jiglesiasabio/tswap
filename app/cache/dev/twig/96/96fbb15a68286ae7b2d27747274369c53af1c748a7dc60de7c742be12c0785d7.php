<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_7a07f41b59edf61fe220543aa13004698c9f96d7a54b9806d412a44f53c75747 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1b7ea16ed72baae6739a314ccb9e36226309a4abafb37b28cfb89b2eb311620 = $this->env->getExtension("native_profiler");
        $__internal_e1b7ea16ed72baae6739a314ccb9e36226309a4abafb37b28cfb89b2eb311620->enter($__internal_e1b7ea16ed72baae6739a314ccb9e36226309a4abafb37b28cfb89b2eb311620_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_e1b7ea16ed72baae6739a314ccb9e36226309a4abafb37b28cfb89b2eb311620->leave($__internal_e1b7ea16ed72baae6739a314ccb9e36226309a4abafb37b28cfb89b2eb311620_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
