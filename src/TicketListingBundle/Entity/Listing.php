<?php

namespace TicketListingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Listing
 *
 * @ORM\Table(name="listing")
 * @ORM\Entity(repositoryClass="TicketListingBundle\Repository\ListingRepository")
 */
class Listing implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="TicketListingBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, name="user_id")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $sellingPrice;

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="listing", cascade={"persist", "remove"})
     */
    protected $tickets;

    /**
     * Listing constructor.
     */
    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Listing
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Listing
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    /**
     * @param mixed $sellingPrice
     * @return Listing
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param mixed $tickets
     * @return Listing
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;
        return $this;
    }

    public function addTicket(Ticket $ticket)
    {
        $this->tickets->add($ticket);
    }

    public function jsonSerialize()
    {
        return [
            'id'=> $this->getId(),
            'user'=> $this->getUser(),
            'description' => $this->getDescription(),
            'selling_price' => $this->getSellingPrice(),
            'tickets' => $this->getTickets() ? $this->getTickets()->toArray() : []
        ];

    }


    /**
     * Returns true if the listing contains tickets marked as bought
     *
     * @return bool
     */
    public function isBought()
    {
        $bought = false;
        /** @var Ticket $ticket */
        foreach ($this->getTickets() as $ticket) {
            $bought = $bought || ($ticket->getBoughtAt() !== null);
        }

        return $bought;
    }

}
