<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_5d95683d7fbed0e211febb3ef3f460591aa9a83cfd70849b8a4d79188181393e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0844354036f96692f28c0c82b056377bb0bdcec76ab12a39353483cd24a89874 = $this->env->getExtension("native_profiler");
        $__internal_0844354036f96692f28c0c82b056377bb0bdcec76ab12a39353483cd24a89874->enter($__internal_0844354036f96692f28c0c82b056377bb0bdcec76ab12a39353483cd24a89874_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0844354036f96692f28c0c82b056377bb0bdcec76ab12a39353483cd24a89874->leave($__internal_0844354036f96692f28c0c82b056377bb0bdcec76ab12a39353483cd24a89874_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_dc1a0461e5371b3d1fb151c940e379961e39236533cecec4e278f45dcb4ca1b4 = $this->env->getExtension("native_profiler");
        $__internal_dc1a0461e5371b3d1fb151c940e379961e39236533cecec4e278f45dcb4ca1b4->enter($__internal_dc1a0461e5371b3d1fb151c940e379961e39236533cecec4e278f45dcb4ca1b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_dc1a0461e5371b3d1fb151c940e379961e39236533cecec4e278f45dcb4ca1b4->leave($__internal_dc1a0461e5371b3d1fb151c940e379961e39236533cecec4e278f45dcb4ca1b4_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ab7c7d23b243e0c7432c0a971c7452e1e0e87053a72e86bf02988a6d74eaad67 = $this->env->getExtension("native_profiler");
        $__internal_ab7c7d23b243e0c7432c0a971c7452e1e0e87053a72e86bf02988a6d74eaad67->enter($__internal_ab7c7d23b243e0c7432c0a971c7452e1e0e87053a72e86bf02988a6d74eaad67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ab7c7d23b243e0c7432c0a971c7452e1e0e87053a72e86bf02988a6d74eaad67->leave($__internal_ab7c7d23b243e0c7432c0a971c7452e1e0e87053a72e86bf02988a6d74eaad67_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_327957655c03459d0f1f1e60dc4007bbe5282534566e357cf807b164a9c6351e = $this->env->getExtension("native_profiler");
        $__internal_327957655c03459d0f1f1e60dc4007bbe5282534566e357cf807b164a9c6351e->enter($__internal_327957655c03459d0f1f1e60dc4007bbe5282534566e357cf807b164a9c6351e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_327957655c03459d0f1f1e60dc4007bbe5282534566e357cf807b164a9c6351e->leave($__internal_327957655c03459d0f1f1e60dc4007bbe5282534566e357cf807b164a9c6351e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
