<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_3193e23685a21c9d4664de472aaffcfaeaff38738ed97681f28eac79b070b71b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1cb4fff5de522c208a85df248604cdcf44193a6138b50769ade31141a2f8f538 = $this->env->getExtension("native_profiler");
        $__internal_1cb4fff5de522c208a85df248604cdcf44193a6138b50769ade31141a2f8f538->enter($__internal_1cb4fff5de522c208a85df248604cdcf44193a6138b50769ade31141a2f8f538_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_1cb4fff5de522c208a85df248604cdcf44193a6138b50769ade31141a2f8f538->leave($__internal_1cb4fff5de522c208a85df248604cdcf44193a6138b50769ade31141a2f8f538_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
