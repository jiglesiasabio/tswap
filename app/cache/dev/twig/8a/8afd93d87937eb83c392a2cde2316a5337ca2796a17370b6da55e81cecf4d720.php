<?php

/* WebAppBundle:Default:users.html.twig */
class __TwigTemplate_2ed545f51534fa8b1b0472a86feb13ff4e6cf477e3b93ba047123967fc144f1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:formPage.html.twig", "WebAppBundle:Default:users.html.twig", 1);
        $this->blocks = array(
            'description' => array($this, 'block_description'),
            'form' => array($this, 'block_form'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:formPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbca4603dee47553ab25ef8783a56e886292fb362c6dd413503d40e8ae3d8a0f = $this->env->getExtension("native_profiler");
        $__internal_bbca4603dee47553ab25ef8783a56e886292fb362c6dd413503d40e8ae3d8a0f->enter($__internal_bbca4603dee47553ab25ef8783a56e886292fb362c6dd413503d40e8ae3d8a0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bbca4603dee47553ab25ef8783a56e886292fb362c6dd413503d40e8ae3d8a0f->leave($__internal_bbca4603dee47553ab25ef8783a56e886292fb362c6dd413503d40e8ae3d8a0f_prof);

    }

    // line 3
    public function block_description($context, array $blocks = array())
    {
        $__internal_5ace86d65376c37fc3badce6857e332c78cbb1c8d2a38c2f1d8555b139a0ab91 = $this->env->getExtension("native_profiler");
        $__internal_5ace86d65376c37fc3badce6857e332c78cbb1c8d2a38c2f1d8555b139a0ab91->enter($__internal_5ace86d65376c37fc3badce6857e332c78cbb1c8d2a38c2f1d8555b139a0ab91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 4
        echo "    <h3>Users</h3>
    <p>To create a user specify the name.</p>
";
        
        $__internal_5ace86d65376c37fc3badce6857e332c78cbb1c8d2a38c2f1d8555b139a0ab91->leave($__internal_5ace86d65376c37fc3badce6857e332c78cbb1c8d2a38c2f1d8555b139a0ab91_prof);

    }

    // line 8
    public function block_form($context, array $blocks = array())
    {
        $__internal_4738e43a031eb81170854541356002f41ee481df2eb189915b1e2fc05a903387 = $this->env->getExtension("native_profiler");
        $__internal_4738e43a031eb81170854541356002f41ee481df2eb189915b1e2fc05a903387->enter($__internal_4738e43a031eb81170854541356002f41ee481df2eb189915b1e2fc05a903387_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 9
        echo "    <form id=\"user-form\">
        Name:<br>
        <input type=\"text\" id=\"name\">
    </form>
";
        
        $__internal_4738e43a031eb81170854541356002f41ee481df2eb189915b1e2fc05a903387->leave($__internal_4738e43a031eb81170854541356002f41ee481df2eb189915b1e2fc05a903387_prof);

    }

    // line 16
    public function block_js($context, array $blocks = array())
    {
        $__internal_213e3756954d6bd7bf614d0c16449db3609e7d67d32d4dae98878bb5131e44dd = $this->env->getExtension("native_profiler");
        $__internal_213e3756954d6bd7bf614d0c16449db3609e7d67d32d4dae98878bb5131e44dd->enter($__internal_213e3756954d6bd7bf614d0c16449db3609e7d67d32d4dae98878bb5131e44dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        // line 17
        echo "    <script>
        function printResponse(data) {
            \$('#console').html(JSON.stringify(data, null, 2));
        }
        \$(document).ready(function () {
            \$('#post').click(function () {
                var name = \$('#name').val();
                if (name) {
                    \$.ajax({
                        type: \"POST\",
                        url: \"/user\",
                        data: {\"name\": name},
                        success: function (data) {
                            printResponse(data)
                        },
                        error: function (data) {
                            printResponse(data.responseJSON)
                        },
                        dataType: \"json\"
                    });
                }
            });
        });
    </script>
";
        
        $__internal_213e3756954d6bd7bf614d0c16449db3609e7d67d32d4dae98878bb5131e44dd->leave($__internal_213e3756954d6bd7bf614d0c16449db3609e7d67d32d4dae98878bb5131e44dd_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 17,  68 => 16,  57 => 9,  51 => 8,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:formPage.html.twig' %}*/
/* */
/* {% block description %}*/
/*     <h3>Users</h3>*/
/*     <p>To create a user specify the name.</p>*/
/* {% endblock %}*/
/* */
/* {% block form %}*/
/*     <form id="user-form">*/
/*         Name:<br>*/
/*         <input type="text" id="name">*/
/*     </form>*/
/* {% endblock %}*/
/* */
/* */
/* {% block js %}*/
/*     <script>*/
/*         function printResponse(data) {*/
/*             $('#console').html(JSON.stringify(data, null, 2));*/
/*         }*/
/*         $(document).ready(function () {*/
/*             $('#post').click(function () {*/
/*                 var name = $('#name').val();*/
/*                 if (name) {*/
/*                     $.ajax({*/
/*                         type: "POST",*/
/*                         url: "/user",*/
/*                         data: {"name": name},*/
/*                         success: function (data) {*/
/*                             printResponse(data)*/
/*                         },*/
/*                         error: function (data) {*/
/*                             printResponse(data.responseJSON)*/
/*                         },*/
/*                         dataType: "json"*/
/*                     });*/
/*                 }*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
