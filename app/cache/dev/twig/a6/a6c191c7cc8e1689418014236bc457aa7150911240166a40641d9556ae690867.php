<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_c6962630f50404af6fe2ab9b29ae36c49fb06a6da7bf606b9202a12be06c5a4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f84dad049a6fa9aa9ff1210945d97e647de9ebeb53141ae4da544461a83df12 = $this->env->getExtension("native_profiler");
        $__internal_6f84dad049a6fa9aa9ff1210945d97e647de9ebeb53141ae4da544461a83df12->enter($__internal_6f84dad049a6fa9aa9ff1210945d97e647de9ebeb53141ae4da544461a83df12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_6f84dad049a6fa9aa9ff1210945d97e647de9ebeb53141ae4da544461a83df12->leave($__internal_6f84dad049a6fa9aa9ff1210945d97e647de9ebeb53141ae4da544461a83df12_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
