<?php

/* ::base.html.twig */
class __TwigTemplate_76fee94356a3abe1e67a97aa65ca1b33de9532071dc11599156546f7ead019dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d5893a8580e973e6a9cbd47ec47b3954fab45dbcac94fa80363ba0c4201cdbe = $this->env->getExtension("native_profiler");
        $__internal_3d5893a8580e973e6a9cbd47ec47b3954fab45dbcac94fa80363ba0c4201cdbe->enter($__internal_3d5893a8580e973e6a9cbd47ec47b3954fab45dbcac94fa80363ba0c4201cdbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_3d5893a8580e973e6a9cbd47ec47b3954fab45dbcac94fa80363ba0c4201cdbe->leave($__internal_3d5893a8580e973e6a9cbd47ec47b3954fab45dbcac94fa80363ba0c4201cdbe_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_194fd26f5b1f6e821269861eea04bd79e2578729bbb71fa5c2922d12eaa5e370 = $this->env->getExtension("native_profiler");
        $__internal_194fd26f5b1f6e821269861eea04bd79e2578729bbb71fa5c2922d12eaa5e370->enter($__internal_194fd26f5b1f6e821269861eea04bd79e2578729bbb71fa5c2922d12eaa5e370_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_194fd26f5b1f6e821269861eea04bd79e2578729bbb71fa5c2922d12eaa5e370->leave($__internal_194fd26f5b1f6e821269861eea04bd79e2578729bbb71fa5c2922d12eaa5e370_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8e7336ee90120506ef88926892104a659af67efa2fdd09ebf189824c94454581 = $this->env->getExtension("native_profiler");
        $__internal_8e7336ee90120506ef88926892104a659af67efa2fdd09ebf189824c94454581->enter($__internal_8e7336ee90120506ef88926892104a659af67efa2fdd09ebf189824c94454581_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_8e7336ee90120506ef88926892104a659af67efa2fdd09ebf189824c94454581->leave($__internal_8e7336ee90120506ef88926892104a659af67efa2fdd09ebf189824c94454581_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_425482b234c9ad994b4e3f03b9b62f560cb84104a738895d486cf5da8cec21b7 = $this->env->getExtension("native_profiler");
        $__internal_425482b234c9ad994b4e3f03b9b62f560cb84104a738895d486cf5da8cec21b7->enter($__internal_425482b234c9ad994b4e3f03b9b62f560cb84104a738895d486cf5da8cec21b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_425482b234c9ad994b4e3f03b9b62f560cb84104a738895d486cf5da8cec21b7->leave($__internal_425482b234c9ad994b4e3f03b9b62f560cb84104a738895d486cf5da8cec21b7_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0a0faf5ffee40f2153d6cca58216eaf0c77ed6bcc3f9595513170256a30ee7bf = $this->env->getExtension("native_profiler");
        $__internal_0a0faf5ffee40f2153d6cca58216eaf0c77ed6bcc3f9595513170256a30ee7bf->enter($__internal_0a0faf5ffee40f2153d6cca58216eaf0c77ed6bcc3f9595513170256a30ee7bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_0a0faf5ffee40f2153d6cca58216eaf0c77ed6bcc3f9595513170256a30ee7bf->leave($__internal_0a0faf5ffee40f2153d6cca58216eaf0c77ed6bcc3f9595513170256a30ee7bf_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
