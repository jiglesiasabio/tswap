<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 06/07/16
 * Time: 23:55
 */

namespace TicketListingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * A ticket cannot have duplicated barcodes
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Ticket implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="TicketListingBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, name="buyer_user_id")
     */
    private $boughtByUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $boughtAt;

    /**
     * @ORM\ManyToOne(targetEntity="Listing", inversedBy="tickets")
     */
    protected $listing;

    /**
     * @ORM\OneToMany(targetEntity="Barcode", mappedBy="ticket", cascade={"persist", "remove"})
     */
    protected $barcodes;

    /**
     * Ticket constructor.
     * @param $barcodes
     */
    public function __construct()
    {
        $this->barcodes = new ArrayCollection();
    }

    public function addBarcode(Barcode $barcode)
    {
        $this->barcodes->add($barcode);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Ticket
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBoughtByUser()
    {
        return $this->boughtByUser;
    }

    /**
     * @param mixed $boughtByUser
     * @return Ticket
     */
    public function setBoughtByUser($boughtByUser)
    {
        $this->boughtByUser = $boughtByUser;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Ticket
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBoughtAt()
    {
        return $this->boughtAt;
    }

    /**
     * @param \DateTime $boughtAt
     * @return Ticket
     */
    public function setBoughtAt($boughtAt)
    {
        $this->boughtAt = $boughtAt;
        return $this;
    }

    /**
     * @return User
     */
    public function getListing()
    {
        return $this->listing;
    }

    /**
     * @param User $listing
     * @return Ticket
     */
    public function setListing($listing)
    {
        $this->listing = $listing;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getBarcodes()
    {
        return $this->barcodes;
    }

    /**
     * @param mixed $barcodes
     * @return Ticket
     */
    public function setBarcodes($barcodes)
    {
        $this->barcodes = $barcodes;
        return $this;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'created_at' => $this->getCreatedAt(),
            'bought_at' => $this->getBoughtAt(),
            'bought_by_user' => $this->getBoughtByUser(),
            'barcodes' => $this->getBarcodes()->toArray()
        ];
    }

}
