<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_42f5a5496a27fc19c21eb7e97c128a22d14209ab3acfc948e9cc9f9ce56c9578 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_056b66efb58819b79e813eb21224a547b58b65488fe567423b061e78887d1ff7 = $this->env->getExtension("native_profiler");
        $__internal_056b66efb58819b79e813eb21224a547b58b65488fe567423b061e78887d1ff7->enter($__internal_056b66efb58819b79e813eb21224a547b58b65488fe567423b061e78887d1ff7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_056b66efb58819b79e813eb21224a547b58b65488fe567423b061e78887d1ff7->leave($__internal_056b66efb58819b79e813eb21224a547b58b65488fe567423b061e78887d1ff7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
