<?php

/* WebAppBundle:Default:listings.html.twig */
class __TwigTemplate_f12e2f64e79a0b90bb46d18a956c231bdd5a086f0820787eb1ab9aa11326c9dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:formPage.html.twig", "WebAppBundle:Default:listings.html.twig", 1);
        $this->blocks = array(
            'description' => array($this, 'block_description'),
            'form' => array($this, 'block_form'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:formPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b050ebd6c81e3119fd05103f340b28474c41f27b68aa8821f1974deb508b725b = $this->env->getExtension("native_profiler");
        $__internal_b050ebd6c81e3119fd05103f340b28474c41f27b68aa8821f1974deb508b725b->enter($__internal_b050ebd6c81e3119fd05103f340b28474c41f27b68aa8821f1974deb508b725b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:listings.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b050ebd6c81e3119fd05103f340b28474c41f27b68aa8821f1974deb508b725b->leave($__internal_b050ebd6c81e3119fd05103f340b28474c41f27b68aa8821f1974deb508b725b_prof);

    }

    // line 3
    public function block_description($context, array $blocks = array())
    {
        $__internal_5ad3854faee309c0f02c3c4357f010e46f03e12439956188b10924e78e1fea82 = $this->env->getExtension("native_profiler");
        $__internal_5ad3854faee309c0f02c3c4357f010e46f03e12439956188b10924e78e1fea82->enter($__internal_5ad3854faee309c0f02c3c4357f010e46f03e12439956188b10924e78e1fea82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 4
        echo "    <h3>Listings</h3>
    <p>To create a listing specify the following data.</p>
";
        
        $__internal_5ad3854faee309c0f02c3c4357f010e46f03e12439956188b10924e78e1fea82->leave($__internal_5ad3854faee309c0f02c3c4357f010e46f03e12439956188b10924e78e1fea82_prof);

    }

    // line 8
    public function block_form($context, array $blocks = array())
    {
        $__internal_2bc2dcd2bbac14a9f69a79bae4a261d414b1b038ce427a508cf2ef8c2ca37735 = $this->env->getExtension("native_profiler");
        $__internal_2bc2dcd2bbac14a9f69a79bae4a261d414b1b038ce427a508cf2ef8c2ca37735->enter($__internal_2bc2dcd2bbac14a9f69a79bae4a261d414b1b038ce427a508cf2ef8c2ca37735_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 9
        echo "    <form id=\"user-form\">
        user id:<br>
        <input type=\"text\" id=\"userId\"><br>

        Selling price:<br>
        <input type=\"text\" id=\"sellingPrice\"><br>

        Description:<br>
        <input type=\"text\" id=\"description\"><br>

        Barcodes of the 1st ticket (comma separated)<br>
        <input type=\"text\" id=\"barcodes_1\"><br>

        Barcodes of the 2nd ticket (comma separated)<br>
        <input type=\"text\" id=\"barcodes_2\"><br>

        Barcodes of the 3rd ticket (comma separated)<br>
        <input type=\"text\" id=\"barcodes_3\"><br>

        <hr>
        <p>Note: The api allows listings to be created with n tickets, but this super simple form does not...</p>
    </form>
";
        
        $__internal_2bc2dcd2bbac14a9f69a79bae4a261d414b1b038ce427a508cf2ef8c2ca37735->leave($__internal_2bc2dcd2bbac14a9f69a79bae4a261d414b1b038ce427a508cf2ef8c2ca37735_prof);

    }

    // line 33
    public function block_js($context, array $blocks = array())
    {
        $__internal_9dfa5f50128f016eb79e9d27a6b72547932e9fe4d389f72e841b4578ac5fdd50 = $this->env->getExtension("native_profiler");
        $__internal_9dfa5f50128f016eb79e9d27a6b72547932e9fe4d389f72e841b4578ac5fdd50->enter($__internal_9dfa5f50128f016eb79e9d27a6b72547932e9fe4d389f72e841b4578ac5fdd50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        // line 34
        echo "    <script>
        function printResponse(data) {
            \$('#console').html(JSON.stringify(data, null, 2));
        }
        \$(document).ready(function () {
            \$('#post').click(function () {

                var userId = \$('#userId').val();
                var sellingPrice = \$('#sellingPrice').val();
                var description = \$('#description').val();
                var barcodes_1 = \$('#barcodes_1').val() ? \$('#barcodes_1').val().split(',') : null;
                var barcodes_2 = \$('#barcodes_2').val() ? \$('#barcodes_2').val().split(',') : null;
                var barcodes_3 = \$('#barcodes_3').val() ? \$('#barcodes_3').val().split(',') : null;

                var data = {
                    \"user_id\": userId,
                    \"selling_price\": sellingPrice,
                    \"description\": description,
                    \"tickets\": []
                };

                if (barcodes_1) { data.tickets.push({'barcodes': barcodes_1}); }
                if (barcodes_2) { data.tickets.push({'barcodes': barcodes_2}); }
                if (barcodes_3) { data.tickets.push({'barcodes': barcodes_3}); }


                if (userId && sellingPrice && description && barcodes_1) {
                    \$.ajax({
                        type: \"POST\",
                        url: \"/listing\",
                        data: data,
                        success: function (data) {
                            printResponse(data)
                        },
                        error: function (data) {
                            printResponse(data.responseJSON)
                        }
                    });
                }
            });
        });
    </script>
";
        
        $__internal_9dfa5f50128f016eb79e9d27a6b72547932e9fe4d389f72e841b4578ac5fdd50->leave($__internal_9dfa5f50128f016eb79e9d27a6b72547932e9fe4d389f72e841b4578ac5fdd50_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:listings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 34,  86 => 33,  57 => 9,  51 => 8,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:formPage.html.twig' %}*/
/* */
/* {% block description %}*/
/*     <h3>Listings</h3>*/
/*     <p>To create a listing specify the following data.</p>*/
/* {% endblock %}*/
/* */
/* {% block form %}*/
/*     <form id="user-form">*/
/*         user id:<br>*/
/*         <input type="text" id="userId"><br>*/
/* */
/*         Selling price:<br>*/
/*         <input type="text" id="sellingPrice"><br>*/
/* */
/*         Description:<br>*/
/*         <input type="text" id="description"><br>*/
/* */
/*         Barcodes of the 1st ticket (comma separated)<br>*/
/*         <input type="text" id="barcodes_1"><br>*/
/* */
/*         Barcodes of the 2nd ticket (comma separated)<br>*/
/*         <input type="text" id="barcodes_2"><br>*/
/* */
/*         Barcodes of the 3rd ticket (comma separated)<br>*/
/*         <input type="text" id="barcodes_3"><br>*/
/* */
/*         <hr>*/
/*         <p>Note: The api allows listings to be created with n tickets, but this super simple form does not...</p>*/
/*     </form>*/
/* {% endblock %}*/
/* */
/* {% block js %}*/
/*     <script>*/
/*         function printResponse(data) {*/
/*             $('#console').html(JSON.stringify(data, null, 2));*/
/*         }*/
/*         $(document).ready(function () {*/
/*             $('#post').click(function () {*/
/* */
/*                 var userId = $('#userId').val();*/
/*                 var sellingPrice = $('#sellingPrice').val();*/
/*                 var description = $('#description').val();*/
/*                 var barcodes_1 = $('#barcodes_1').val() ? $('#barcodes_1').val().split(',') : null;*/
/*                 var barcodes_2 = $('#barcodes_2').val() ? $('#barcodes_2').val().split(',') : null;*/
/*                 var barcodes_3 = $('#barcodes_3').val() ? $('#barcodes_3').val().split(',') : null;*/
/* */
/*                 var data = {*/
/*                     "user_id": userId,*/
/*                     "selling_price": sellingPrice,*/
/*                     "description": description,*/
/*                     "tickets": []*/
/*                 };*/
/* */
/*                 if (barcodes_1) { data.tickets.push({'barcodes': barcodes_1}); }*/
/*                 if (barcodes_2) { data.tickets.push({'barcodes': barcodes_2}); }*/
/*                 if (barcodes_3) { data.tickets.push({'barcodes': barcodes_3}); }*/
/* */
/* */
/*                 if (userId && sellingPrice && description && barcodes_1) {*/
/*                     $.ajax({*/
/*                         type: "POST",*/
/*                         url: "/listing",*/
/*                         data: data,*/
/*                         success: function (data) {*/
/*                             printResponse(data)*/
/*                         },*/
/*                         error: function (data) {*/
/*                             printResponse(data.responseJSON)*/
/*                         }*/
/*                     });*/
/*                 }*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
