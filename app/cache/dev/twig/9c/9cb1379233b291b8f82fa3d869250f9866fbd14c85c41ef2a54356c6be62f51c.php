<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_6b8ad5317caa099c5ea678da38d3695c5c265408753ee1fbf1e1e91566ad4242 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26c5f1b4ac1a5bb9a99ecdb30bfcfc6a569b285b94ccfcca0632816d37782a56 = $this->env->getExtension("native_profiler");
        $__internal_26c5f1b4ac1a5bb9a99ecdb30bfcfc6a569b285b94ccfcca0632816d37782a56->enter($__internal_26c5f1b4ac1a5bb9a99ecdb30bfcfc6a569b285b94ccfcca0632816d37782a56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_26c5f1b4ac1a5bb9a99ecdb30bfcfc6a569b285b94ccfcca0632816d37782a56->leave($__internal_26c5f1b4ac1a5bb9a99ecdb30bfcfc6a569b285b94ccfcca0632816d37782a56_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
