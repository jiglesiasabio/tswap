# TS assessment
 
jiglesia

# Requirements

Vagrant and virtual box

# Install:

git clone git@bitbucket.org:jiglesiasabio/tswap.git

cd tswap

cd vagrant

vagrant up

# Test:

Navigate to : http://192.168.33.10/app/

# DB Access:

With any mySQL client that supports ssh connections (i.e. mySQL workbench)

__Host:__

192.168.33.10

__SSH credentials:__

u: vagrant

p: vagrant

__mySQL credentials__

u: root

p: root

# The Stack:

* LEMP Stack with ubuntu
* Vagrant box shell provisioned
* Symfony 2.8

# Improvement areas:

* Frontend part is ultra simple and ad hoc (but it was assumed it wasn't the main task)
* Asset management is not present
* Error format is not stardardized in all cases (in most of them, but not all)
* Input validation is not very strong in some cases
* .gitignore (everything commited for convenience of the reviewers)