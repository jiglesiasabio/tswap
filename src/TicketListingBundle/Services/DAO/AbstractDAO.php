<?php

namespace TicketListingBundle\Services\DAO;
use Doctrine\ORM\EntityManager;
use TicketListingBundle\Entity\User;

/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 00:29
 */
abstract class AbstractDAO
{
    abstract public function __construct(EntityManager $entityManager);

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function save($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }
}