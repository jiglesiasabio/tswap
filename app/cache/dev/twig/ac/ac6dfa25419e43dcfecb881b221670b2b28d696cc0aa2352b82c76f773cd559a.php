<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_3e114cfe63eb2818392b4b2d5da7f4e38b0a219e9be656d20a1ac231cf5e7ba3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40ca97cde4f4ff336e7bb306cfcf3463a00a07558139185654377cff9a9dd1fb = $this->env->getExtension("native_profiler");
        $__internal_40ca97cde4f4ff336e7bb306cfcf3463a00a07558139185654377cff9a9dd1fb->enter($__internal_40ca97cde4f4ff336e7bb306cfcf3463a00a07558139185654377cff9a9dd1fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_40ca97cde4f4ff336e7bb306cfcf3463a00a07558139185654377cff9a9dd1fb->leave($__internal_40ca97cde4f4ff336e7bb306cfcf3463a00a07558139185654377cff9a9dd1fb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
