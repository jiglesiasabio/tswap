<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_1c624c226d1985ae15c4e7ce67e41e3f311d1700b67b3c0e0a1d15235f00e238 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_406f14cea4c6494aac436125fb93060717e24796b0eb629ddbf4053730c60122 = $this->env->getExtension("native_profiler");
        $__internal_406f14cea4c6494aac436125fb93060717e24796b0eb629ddbf4053730c60122->enter($__internal_406f14cea4c6494aac436125fb93060717e24796b0eb629ddbf4053730c60122_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_406f14cea4c6494aac436125fb93060717e24796b0eb629ddbf4053730c60122->leave($__internal_406f14cea4c6494aac436125fb93060717e24796b0eb629ddbf4053730c60122_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
