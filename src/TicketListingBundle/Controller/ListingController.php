<?php
/**
 * Created by PhpStorm.
 * User: jiglesia
 * Date: 07/07/16
 * Time: 09:52
 */

namespace TicketListingBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as API;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use TicketListingBundle\Entity\Listing;
use TicketListingBundle\Entity\User;
use TicketListingBundle\Services\DAO\UserDAO;
use TicketListingBundle\Services\ListingService;

/**
 * @Route("/listing")
 */
class ListingController extends BaseController
{

    /**
     * Gets the user from the 'user_id' in the request.
     * If the user is not valid it throws an Auth exception.
     *
     * @param Request $request
     * @return mixed
     */
    private function getUserFromRequest(Request $request)
    {
        /** @var UserDAO $userDAO */
        $userDAO = $this->get('ticket_listing.user.dao');
        $userId = $request->request->get('user_id');

        $user = $userDAO->find($userId);

        if ($user) {
            return $user;
        } else {
            Throw new UnauthorizedHttpException("The provider user is not valid");
        }
    }

    /**
     * Endpoint for creating a new Ticket Listing.
     *
     * @API\Post("", name="listing_post")
     * @API\Post("/", name="listing_post_slashed")
     *
     * @API\RequestParam(name="user_id", nullable=false)
     * @API\RequestParam(name="selling_price", nullable=false)
     * @API\RequestParam(name="description", nullable=false)
     */
    public function createListingAction(Request $request)
    {
        $user = $this->getUserFromRequest($request);

        $sellingPrice = $request->request->get('selling_price');
        $description = $request->request->get('description');
        $tickets = $request->request->get('tickets');

        /** @var ListingService $listingService */
        $listingService = $this->get('ticket_listing.listing.service');

        try {
            $listing = $listingService->createListing($user, $sellingPrice, $description, $tickets);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }


        return $this->successResponse($listing);

    }

    /**
     * Endpoint for creating a new Ticket Listing.
     *
     * @API\Post("/{id}/buy", name="buy_listing_post")
     * @API\Post("/{id}/buy/", name="buy_listing_post_slashed")
     *
     * @API\RequestParam(name="user_id", nullable=false)
     */
    public function buyListingAction(Request $request, Listing $listing)
    {
        $user = $this->getUserFromRequest($request);

        /** @var ListingService $listingService */
        $listingService = $this->get('ticket_listing.listing.service');

        try {
            $listing = $listingService->buyListing($listing, $user);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }


        return $this->successResponse($listing);
    }
}