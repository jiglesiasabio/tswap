<?php

/* WebAppBundle:Default:formPage.html.twig */
class __TwigTemplate_b7ecc47160e0f0d2209a729529655356f4784baa3f967bfe051fdb2123109251 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebAppBundle:Default:baseTemplate.html.twig", "WebAppBundle:Default:formPage.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'description' => array($this, 'block_description'),
            'form' => array($this, 'block_form'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebAppBundle:Default:baseTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_afcb0683e2969dcc5d9f380cda5a707ac99fde8fa492739b7d35ad9143e7f77b = $this->env->getExtension("native_profiler");
        $__internal_afcb0683e2969dcc5d9f380cda5a707ac99fde8fa492739b7d35ad9143e7f77b->enter($__internal_afcb0683e2969dcc5d9f380cda5a707ac99fde8fa492739b7d35ad9143e7f77b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebAppBundle:Default:formPage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_afcb0683e2969dcc5d9f380cda5a707ac99fde8fa492739b7d35ad9143e7f77b->leave($__internal_afcb0683e2969dcc5d9f380cda5a707ac99fde8fa492739b7d35ad9143e7f77b_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_ead557b7a7c70d581bceb7a21d73cf7edd5679d65be097a2b7428ce33a02b062 = $this->env->getExtension("native_profiler");
        $__internal_ead557b7a7c70d581bceb7a21d73cf7edd5679d65be097a2b7428ce33a02b062->enter($__internal_ead557b7a7c70d581bceb7a21d73cf7edd5679d65be097a2b7428ce33a02b062_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"row marketing\">
        <div class=\"col-lg-12\">

            ";
        // line 7
        $this->displayBlock('description', $context, $blocks);
        // line 9
        echo "
            ";
        // line 10
        $this->displayBlock('form', $context, $blocks);
        // line 13
        echo "
            <hr>

            <button type=\"button\" class=\"btn btn-default btn-sm\" id=\"post\">
                <span class=\"glyphicon glyphicon-play\" aria-hidden=\"true\"></span> POST
            </button>

            <hr>

        </div>

        <div class=\"col-lg-12\">
            <pre id=\"console\">Responses will appear here...</pre>
        </div>
    </div>

    ";
        // line 29
        $this->displayBlock('js', $context, $blocks);
        
        $__internal_ead557b7a7c70d581bceb7a21d73cf7edd5679d65be097a2b7428ce33a02b062->leave($__internal_ead557b7a7c70d581bceb7a21d73cf7edd5679d65be097a2b7428ce33a02b062_prof);

    }

    // line 7
    public function block_description($context, array $blocks = array())
    {
        $__internal_98cf564c58df04fbe2ffcfc448758da8723f2128311553e91893996804d65003 = $this->env->getExtension("native_profiler");
        $__internal_98cf564c58df04fbe2ffcfc448758da8723f2128311553e91893996804d65003->enter($__internal_98cf564c58df04fbe2ffcfc448758da8723f2128311553e91893996804d65003_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "description"));

        // line 8
        echo "            ";
        
        $__internal_98cf564c58df04fbe2ffcfc448758da8723f2128311553e91893996804d65003->leave($__internal_98cf564c58df04fbe2ffcfc448758da8723f2128311553e91893996804d65003_prof);

    }

    // line 10
    public function block_form($context, array $blocks = array())
    {
        $__internal_d079555f7de191d097751ccf1fc619191f8540170889909123223e0668757dd8 = $this->env->getExtension("native_profiler");
        $__internal_d079555f7de191d097751ccf1fc619191f8540170889909123223e0668757dd8->enter($__internal_d079555f7de191d097751ccf1fc619191f8540170889909123223e0668757dd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 11
        echo "
            ";
        
        $__internal_d079555f7de191d097751ccf1fc619191f8540170889909123223e0668757dd8->leave($__internal_d079555f7de191d097751ccf1fc619191f8540170889909123223e0668757dd8_prof);

    }

    // line 29
    public function block_js($context, array $blocks = array())
    {
        $__internal_6e191e6a06c92207f35eef372c416ad85a566021109130da5a8640cca46b9ea6 = $this->env->getExtension("native_profiler");
        $__internal_6e191e6a06c92207f35eef372c416ad85a566021109130da5a8640cca46b9ea6->enter($__internal_6e191e6a06c92207f35eef372c416ad85a566021109130da5a8640cca46b9ea6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        // line 30
        echo "
    ";
        
        $__internal_6e191e6a06c92207f35eef372c416ad85a566021109130da5a8640cca46b9ea6->leave($__internal_6e191e6a06c92207f35eef372c416ad85a566021109130da5a8640cca46b9ea6_prof);

    }

    public function getTemplateName()
    {
        return "WebAppBundle:Default:formPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 30,  107 => 29,  99 => 11,  93 => 10,  86 => 8,  80 => 7,  73 => 29,  55 => 13,  53 => 10,  50 => 9,  48 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'WebAppBundle:Default:baseTemplate.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <div class="row marketing">*/
/*         <div class="col-lg-12">*/
/* */
/*             {% block description %}*/
/*             {% endblock %}*/
/* */
/*             {% block form %}*/
/* */
/*             {% endblock %}*/
/* */
/*             <hr>*/
/* */
/*             <button type="button" class="btn btn-default btn-sm" id="post">*/
/*                 <span class="glyphicon glyphicon-play" aria-hidden="true"></span> POST*/
/*             </button>*/
/* */
/*             <hr>*/
/* */
/*         </div>*/
/* */
/*         <div class="col-lg-12">*/
/*             <pre id="console">Responses will appear here...</pre>*/
/*         </div>*/
/*     </div>*/
/* */
/*     {% block js %}*/
/* */
/*     {% endblock %}*/
/* {% endblock %}*/
